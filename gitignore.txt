# ignore MacOS system data
*.DS_Store
# ignore cmake user data and build
CMakeLists.txt.user
build-*
# ignore matrices
data/matrices/*.mlcuda
# ignore binaries
bin/matgen
bin/matprint
bin/benchmark

