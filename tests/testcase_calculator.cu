#include <iostream>
#include "../ml_cuda/ml_cuda.hpp"
#include "../kernels/tunable/matmul.cuh"

#define ML_TYPE0      "td4"
#define CPP_TYPE0     ml_cuda::tangent_t<double,4>

#define ML_TYPE1      "itd4"
#define CPP_TYPE1     ml_cuda::interval_t<ml_cuda::tangent_t<double,4>>

#define TC_FOLDER     "data/testcases/"

#define NUM_BLOCKS    2
#define NUM_THREADS   2

static const std::string fname_IxD_0 = TC_FOLDER "testcase_mat_identity_3_3_" ML_TYPE0 ".mlcuda";
static const std::string fname_IxD_1 = TC_FOLDER "testcase_mat_3_3_" ML_TYPE1 ".mlcuda";

static const std::string fname_DxI_0 = TC_FOLDER "testcase_mat_3_3_" ML_TYPE0 ".mlcuda";
static const std::string fname_DxI_1 = TC_FOLDER "testcase_mat_identity_3_3_" ML_TYPE1 ".mlcuda";

static const std::string fname_DxD_0 = TC_FOLDER "testcase_mat_3_3_" ML_TYPE0 ".mlcuda";
static const std::string fname_DxD_1 = TC_FOLDER "testcase_mat_3_3_" ML_TYPE1 ".mlcuda";

int main() {
  std::cout << "Running testcases for: type0=" ML_TYPE0 " type1=" ML_TYPE1 "\n";

  std::cout << "\n----------------------------------------\n";
  std::cout << "RUN: M=I*D\n";
  ml_cuda::matrix_t<CPP_TYPE0> IxD_0(fname_IxD_0.c_str(), true);
  std::cout << "\nMatrix I:\n" << IxD_0;
  ml_cuda::matrix_t<CPP_TYPE1> IxD_1(fname_IxD_1.c_str(), true);
  std::cout << "\nMatrix D:\n" << IxD_1;
  ml_cuda::matrix_t<CPP_TYPE1> IxD_M(IxD_0.rows(), IxD_1.cols(), true);
  std::cout << "\nMatrix M:\n" << IxD_M;
  {
    cudaError_t error;
    error = cudaGetLastError();
    if (error!=0) {
      std::cout << "\nCuda error " << error << " before kernel: "
                << cudaGetErrorString(error) << "\n";
    }
    ml_cuda::matmul_reference<<<NUM_BLOCKS, NUM_THREADS>>>(IxD_0, IxD_1, IxD_M);
    cudaDeviceSynchronize();
    error = cudaGetLastError();
    if (error!=0) {
      std::cout << "\nCuda error " << error << " after kernel: "
                << cudaGetErrorString(error) << "\n";
    } else {
      std::cout << "\nCalculation successful...\n";
    }
  }
  std::cout << "\nMatrix M:\n" << IxD_M;

  std::cout << "\n----------------------------------------\n";
  std::cout << "RUN: M=D*I\n";
  ml_cuda::matrix_t<CPP_TYPE0> DxI_0(fname_DxI_0.c_str(), true);
  std::cout << "\nMatrix D:\n" << DxI_0;
  ml_cuda::matrix_t<CPP_TYPE1> DxI_1(fname_DxI_1.c_str(), true);
  std::cout << "\nMatrix I:\n" << DxI_1;
  ml_cuda::matrix_t<CPP_TYPE1> DxI_M(DxI_0.rows(), DxI_1.cols(), true);
  std::cout << "\nMatrix M:\n" << DxI_M;
  {
    cudaError_t error;
    error = cudaGetLastError();
    if (error!=0) {
      std::cout << "\nCuda error " << error << " before kernel: "
                << cudaGetErrorString(error) << "\n";
    }
    ml_cuda::matmul_reference<<<NUM_BLOCKS, NUM_THREADS>>>(DxI_0, DxI_1, DxI_M);
    cudaDeviceSynchronize();
    error = cudaGetLastError();
    if (error!=0) {
      std::cout << "\nCuda error " << error << " after kernel: "
                << cudaGetErrorString(error) << "\n";
    } else {
      std::cout << "\nCalculation successful...\n";
    }
  }
  std::cout << "\nMatrix M:\n" << DxI_M;

  std::cout << "\n----------------------------------------\n";
  std::cout << "RUN: M=D^t*I\n";
  ml_cuda::matrix_t<CPP_TYPE0> DtxI_0(fname_DxI_0.c_str(), true);
  std::cout << "\nMatrix D:\n" << DtxI_0;
  ml_cuda::matrix_t<CPP_TYPE1> DtxI_1(fname_DxI_1.c_str(), true);
  std::cout << "\nMatrix I:\n" << DtxI_1;
  ml_cuda::matrix_t<CPP_TYPE1> DtxI_M(DxI_0.rows(), DxI_1.cols(), true);
  std::cout << "\nMatrix M:\n" << DtxI_M;
  {
    cudaError_t error;
    error = cudaGetLastError();
    if (error!=0) {
      std::cout << "\nCuda error " << error << " before kernel: "
                << cudaGetErrorString(error) << "\n";
    }
    ml_cuda::matmul_reference<<<NUM_BLOCKS, NUM_THREADS>>>(DtxI_0, DtxI_1, DtxI_M, true);
    cudaDeviceSynchronize();
    error = cudaGetLastError();
    if (error!=0) {
      std::cout << "\nCuda error " << error << " after kernel: "
                << cudaGetErrorString(error) << "\n";
    } else {
      std::cout << "\nCalculation successful...\n";
    }
  }
  std::cout << "\nMatrix M:\n" << DtxI_M;

  std::cout << "\n----------------------------------------\n";
  std::cout << "RUN: M=D1*D2\n";
  ml_cuda::matrix_t<CPP_TYPE0> DxD_0(fname_DxD_0.c_str(), true);
  std::cout << "\nMatrix D1:\n" << DxD_0;
  ml_cuda::matrix_t<CPP_TYPE1> DxD_1(fname_DxD_1.c_str(), true);
  std::cout << "\nMatrix D2:\n" << DxD_1;
  ml_cuda::matrix_t<CPP_TYPE1> DxD_M(DxD_0.rows(), DxD_1.cols(), true);
  std::cout << "\nMatrix M:\n" << DxD_M;
  {
    cudaError_t error;
    error = cudaGetLastError();
    if (error!=0) {
      std::cout << "\nCuda error " << error << " before kernel: "
                << cudaGetErrorString(error) << "\n";
    }
    ml_cuda::matmul_reference<<<NUM_BLOCKS, NUM_THREADS>>>(DxD_0, DxD_1, DxD_M);
    cudaDeviceSynchronize();
    error = cudaGetLastError();
    if (error!=0) {
      std::cout << "\nCuda error " << error << " after kernel: "
                << cudaGetErrorString(error) << "\n";
    } else {
      std::cout << "\nCalculation successful...\n";
    }
  }
  std::cout << "\nMatrix M:\n" << DxD_M;

  std::cout << "\n----------------------------------------\n";
  std::cout << "RUN: M=D1^t*D2\n";
  ml_cuda::matrix_t<CPP_TYPE0> DtxD_0(fname_DxD_0.c_str(), true);
  std::cout << "\nMatrix D1:\n" << DtxD_0;
  ml_cuda::matrix_t<CPP_TYPE1> DtxD_1(fname_DxD_1.c_str(), true);
  std::cout << "\nMatrix D2:\n" << DtxD_1;
  ml_cuda::matrix_t<CPP_TYPE1> DtxD_M(DxD_0.rows(), DxD_1.cols(), true);
  std::cout << "\nMatrix M:\n" << DtxD_M;
  {
    cudaError_t error;
    error = cudaGetLastError();
    if (error!=0) {
      std::cout << "\nCuda error " << error << " before kernel: "
                << cudaGetErrorString(error) << "\n";
    }
    ml_cuda::matmul_reference<<<NUM_BLOCKS, NUM_THREADS>>>(DtxD_0, DtxD_1, DtxD_M, true);
    cudaDeviceSynchronize();
    error = cudaGetLastError();
    if (error!=0) {
      std::cout << "\nCuda error " << error << " after kernel: "
                << cudaGetErrorString(error) << "\n";
    } else {
      std::cout << "\nCalculation successful...\n";
    }
  }
  std::cout << "\nMatrix M:\n" << DtxD_M;
}
