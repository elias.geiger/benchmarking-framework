#include <iostream>

#include "../ml_cuda/ml_cuda.hpp"
#include "../kernels/tunable/matmul.cuh"

#define M 32
#define K 256
#define N 128

#define DELTA double(pow(10, -5))

#define type_A ml_cuda::tangent_t<double,16>
#define sizeof_A ML_CUDA_SIZE_TANGENT_DOUBLE_16
#define type_B ml_cuda::tangent_t<double,16>
#define sizeof_B ML_CUDA_SIZE_TANGENT_DOUBLE_16

#define printMat              false
#define useRefImplementation  false
#define blocks1D              2
#define threads1D             2
#define _block_size_x         7
#define _block_size_y         3
#define _grid_size_x          ceil((double)M/_block_size_x)
#define _grid_size_y          ceil((double)N/_block_size_y)

#define usedSmemSize _block_size_x*_block_size_y*(sizeof_A+sizeof_B)

#define cfg_out "[MMtest:gx="<<_grid_size_x<<",bx="<<_block_size_x<<",gy="<<_grid_size_y<<",by="<<_block_size_y<<"] "

int main() {

  std::cout << _grid_size_x << ", " << _grid_size_y << "\n";

  cudaDeviceSetCacheConfig(cudaFuncCachePreferShared);

  if(!useRefImplementation && usedSmemSize>49152) {
    std::cerr << "Too much shared memory: " << usedSmemSize <<"/49152\n";
    return 1;
  } else {
    std::cout << "Using shared memory: " << usedSmemSize <<"/49152\n";
  }

  ml_cuda::matrix_t<type_A> A(M, K, [](index_t row, index_t col) {
    return (row==col)?1:0;
  }, true);
  if (printMat) std::cout << "\nA\n" << A;

  ml_cuda::matrix_t<type_B> B(K, N, [](index_t row, index_t col) {
    double tangents[16];
    for (int i=0; i<16; ++i) {
      tangents[i] = i;
    }
    return ml_cuda::tangent_t<double,16>(row*4+col+1, tangents);
  }, true);
  if (printMat) std::cout << "\nB\n" << B;
  std::cout << "\nB(0,67):" << B(0,67) << "\n";

  ml_cuda::matrix_t<type_B> C(M, N, true);
  if (printMat) std::cout << "\nC\n" << C;

  ml_cuda::matrix_t<type_B> C_ref(M , N, true);
  if (printMat) std::cout << "\nC\n" << C_ref;

  {
    cudaError_t error = cudaGetLastError();
    if(error!=0) std::cout << "\nCuda error before kernel: " << error << "\n";

    ml_cuda::matmul_reference<<<blocks1D, threads1D>>>(A, B, C_ref);
    #if !(usedSmemSize>49152)
      kernels::matmul<<<dim3(_grid_size_x,_grid_size_y),
        dim3(_block_size_x,_block_size_y)>>>(A, B, C, false, true);//, dbgDump);
    #else
      std::cerr << "Error: Couldn't start custom kernel!\n";
    #endif
    cudaDeviceSynchronize();

    error = cudaGetLastError();
    if (error!=0) {
      std::cout << "\nCuda error " << error << " after kernel: "
                           << cudaGetErrorString(error) << "\n";
    } else {
      std::cout << "Success...\n";
    }
  }
  cudaDeviceSynchronize();

  double max_err = 0;
  for (unsigned int i = 0; i < M; ++i) {
    for (unsigned int j = 0; j < N; ++j) {
      double cur_err = ml_cuda::getError(C(i,j), C_ref(i,j));
      if (max_err < cur_err) max_err = cur_err;
      if (cur_err > 0) {
        std::cout << "DEBUG:"
                  << "\nC    (" << i << "," << j << ")=" << C(i,j)
                  << "\nC_ref(" << i << "," << j <<")=" << C_ref(i,j)
                  << "\ndelta=" << ml_cuda::getError(C(i,j), C_ref(i,j)) << "\n";
      }
    }
  }
  if (max_err > DELTA) {
    std::cerr << cfg_out << "Maximal relative error exceeds " << DELTA << ": " << max_err << "\n";
  }
  else if (max_err != 0){
    std::cout << cfg_out << "Maximal relative error is not zero: " << max_err << "\n";
  }

  if (printMat) std::cout << "\nC after matmul:\n" << C;
}
