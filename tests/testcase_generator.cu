#include <iostream>
#include <sstream>

#include "../ml_cuda/ml_cuda.hpp"

#define ATOMIC_TYPE                           double
#define TYPE                                  ml_cuda::tangent_t<ATOMIC_TYPE>
static const unsigned int ROWS                = 3;
static const unsigned int COLS                = 3;
static const bool IS_IDENTITY                 = true;
static const unsigned int TANGENT_VECTOR_SIZE = 4;
static const ml_cuda::primal_type_t ML_TYPE0  = ml_cuda::TANGENT;
static const ml_cuda::primal_type_t ML_TYPE1  = ml_cuda::DOUBLE;
static const ml_cuda::primal_type_t ML_TYPE2  = ml_cuda::UNSPECIFIED;
static const auto iVAL = [](index_t row, index_t col) {return ATOMIC_TYPE(row*COLS+col+1);};
static const auto iiVAL = [](index_t row, index_t col) {return row==col?ATOMIC_TYPE(1.0):ATOMIC_TYPE(0.0);};

using ml_cuda::interval_t;
using ml_cuda::tangent_t;

int main() {
  //identity t4
  ml_cuda::matrix_t<tangent_t<ATOMIC_TYPE, TANGENT_VECTOR_SIZE>> M(ROWS, COLS, [](index_t row, index_t col){
    ATOMIC_TYPE tangents[TANGENT_VECTOR_SIZE];
    for(unsigned int i = 0; i < TANGENT_VECTOR_SIZE; ++i) {
      tangents[i] = ATOMIC_TYPE(i);
    }
    tangent_t<ATOMIC_TYPE,TANGENT_VECTOR_SIZE> ret(iiVAL(row,col), tangents);
    return ret;
  }, true);
  //dense t4
  /*ml_cuda::matrix_t<tangent_t<ATOMIC_TYPE, TANGENT_VECTOR_SIZE>> M(ROWS, COLS, [](index_t row, index_t col){
    ATOMIC_TYPE tangents[TANGENT_VECTOR_SIZE];
    for(unsigned int i = 0; i < TANGENT_VECTOR_SIZE; ++i) {
      tangents[i] = iVAL(row,col)+i;
    }
    tangent_t<ATOMIC_TYPE, TANGENT_VECTOR_SIZE> ret(iVAL(row,col), tangents);
    return ret;
  }, true);*/
  //identity it
  /*ml_cuda::matrix_t<interval_t<tangent_t<ATOMIC_TYPE, TANGENT_VECTOR_SIZE>>> M(ROWS, COLS, [](index_t row, index_t col){
    ATOMIC_TYPE tangents[TANGENT_VECTOR_SIZE];
    for(unsigned int i = 0; i < TANGENT_VECTOR_SIZE; ++i) {
      tangents[i] = ATOMIC_TYPE(i);
    }
    tangent_t<ATOMIC_TYPE,TANGENT_VECTOR_SIZE> tmax(iiVAL(row,col), tangents);
    tangent_t<ATOMIC_TYPE,TANGENT_VECTOR_SIZE> tmin(0.0, tangents);
    interval_t<tangent_t<ATOMIC_TYPE,TANGENT_VECTOR_SIZE>> ret(tmin, tmax);
    return ret;
  }, true);*/
  //dense it
  /*ml_cuda::matrix_t<interval_t<tangent_t<ATOMIC_TYPE, TANGENT_VECTOR_SIZE>>> M(ROWS, COLS, [](index_t row, index_t col){
    ATOMIC_TYPE tangents[TANGENT_VECTOR_SIZE];
    for(unsigned int i = 0; i < TANGENT_VECTOR_SIZE; ++i) {
      tangents[i] = iVAL(row,col)+i;
    }
    tangent_t<ATOMIC_TYPE,TANGENT_VECTOR_SIZE> tmax(iVAL(row,col), tangents);
    tangent_t<ATOMIC_TYPE,TANGENT_VECTOR_SIZE> tmin(-iVAL(row,col), tangents);
    interval_t<tangent_t<ATOMIC_TYPE, TANGENT_VECTOR_SIZE>> ret(tmin, tmax);
    return ret;
  }, true);*/
  std::cout << "\nGenerated Matrix:\n" << M;

  //  ---------- available types ----------
  //  FLOAT
  //  DOUBLE
  //  INTERVAL
  //  TANGENT
  //  UNSPECIFIED
  ml_cuda::type_t t;
  t.primals[0] = ML_TYPE0;
  t.primals[1] = ML_TYPE1;
  t.primals[2] = ML_TYPE2;
  M.setType(t);

  std::stringstream fname;
  fname << "testcase_mat_";
  if (IS_IDENTITY) fname << "identity" << '_';
  fname << ROWS << '_' << COLS << '_' << t;
  if (TANGENT_VECTOR_SIZE>1) fname << TANGENT_VECTOR_SIZE;
  fname << ".mlcuda";
  std::cout << "\nWriting to file:\n" << fname.str() << "\n";
  M.writeToFile(fname.str().c_str(), TANGENT_VECTOR_SIZE);
}
