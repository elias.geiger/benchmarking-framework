#include "ml_cuda/ml_cuda.hpp"

#define tile_size_x 12
#define tile_size_y 12
#define fetch_size 2
#define type_A float
#define type_B ml_cuda::interval_t<float>

int main() {

  int usedSize = tile_size_x*tile_size_y*(sizeof(type_A)+sizeof(type_B));
  std::cout << "USED SMEM SIZE: " << usedSize << "/49152\n\n";

  std::cout << "sizeof(float)= " << sizeof(float) << "\n";
  std::cout << "sizeof(double)= " << sizeof(double) << "\n";
  std::cout << "sizeof(ml_cuda::interval_t<float>)= " << sizeof(ml_cuda::interval_t<float>) << "\n";
  std::cout << "sizeof(ml_cuda::interval_t<double>)= " << sizeof(ml_cuda::interval_t<double>) << "\n";
  std::cout << "sizeof(ml_cuda::tangent_t<float>)= " << sizeof(ml_cuda::tangent_t<float>) << "\n";
  std::cout << "sizeof(ml_cuda::tangent_t<double>)= " << sizeof(ml_cuda::tangent_t<double>) << "\n";
  std::cout << "sizeof(ml_cuda::interval_t<ml_cuda::tangent_t<float>>)= " << sizeof(ml_cuda::interval_t<ml_cuda::tangent_t<float>>) << "\n";
  std::cout << "sizeof(ml_cuda::interval_t<ml_cuda::tangent_t<double>>)= " << sizeof(ml_cuda::interval_t<ml_cuda::tangent_t<double>>) << "\n";
  std::cout << "sizeof(ml_cuda::tangent_t<float,4>)= " << sizeof(ml_cuda::tangent_t<float,4>) << "\n";
  std::cout << "sizeof(ml_cuda::tangent_t<float,16>)= " << sizeof(ml_cuda::tangent_t<float,16>) << "\n";
  std::cout << "sizeof(ml_cuda::tangent_t<float,64>)= " << sizeof(ml_cuda::tangent_t<float,64>) << "\n";
  std::cout << "sizeof(ml_cuda::tangent_t<double,4>)= " << sizeof(ml_cuda::tangent_t<double,4>) << "\n";
  std::cout << "sizeof(ml_cuda::tangent_t<double,16>)= " << sizeof(ml_cuda::tangent_t<double,16>) << "\n";
  std::cout << "sizeof(ml_cuda::tangent_t<double,64>)= " << sizeof(ml_cuda::tangent_t<double,64>) << "\n";
  std::cout << "sizeof(ml_cuda::interval_t<ml_cuda::tangent_t<float,4>>)= " << sizeof(ml_cuda::interval_t<ml_cuda::tangent_t<float,4>>) << "\n";
  std::cout << "sizeof(ml_cuda::interval_t<ml_cuda::tangent_t<float,16>>)= " << sizeof(ml_cuda::interval_t<ml_cuda::tangent_t<float,16>>) << "\n";
  std::cout << "sizeof(ml_cuda::interval_t<ml_cuda::tangent_t<float,64>>)= " << sizeof(ml_cuda::interval_t<ml_cuda::tangent_t<float,64>>) << "\n";
  std::cout << "sizeof(ml_cuda::interval_t<ml_cuda::tangent_t<double,4>>)= " << sizeof(ml_cuda::interval_t<ml_cuda::tangent_t<double,4>>) << "\n";
  std::cout << "sizeof(ml_cuda::interval_t<ml_cuda::tangent_t<double,16>>)= " << sizeof(ml_cuda::interval_t<ml_cuda::tangent_t<double,16>>) << "\n";
  std::cout << "sizeof(ml_cuda::interval_t<ml_cuda::tangent_t<double,64>>)= " << sizeof(ml_cuda::interval_t<ml_cuda::tangent_t<double,64>>) << "\n";
}
