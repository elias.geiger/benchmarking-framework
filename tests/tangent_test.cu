#include "ml_cuda/ml_cuda.hpp"

int main() {

  ml_cuda::matrix_t<double> values(4, 4, [](ml_cuda::index_t row, ml_cuda::index_t col){return 4*row+col;}, false);
  ml_cuda::matrix_t<double> tangents1(4, 4, [](ml_cuda::index_t row, ml_cuda::index_t col){return 4*row+col+1;}, false);
  ml_cuda::matrix_t<double> tangents2(4, 4, [](ml_cuda::index_t row, ml_cuda::index_t col){return 4*row+col+2;}, false);

  ml_cuda::matrix_t<double> tangents[2] = {tangents1,tangents2};
  ml_cuda::separate_tangent_matrix_t<double, 2> M(values, tangents);

  std::cout << "Matrix values:\n" << values;
  std::cout << "Matrix tangents1:\n" << tangents1;
  std::cout << "Matrix tangents2:\n" << tangents2;
  std::cout << "Matrix M:\n" << M;

  double d2[2] = {3.14, 2.22};
  ml_cuda::tangent_t<double,2> t(.005, d2);

  std::cout << "Reading at M(2,2): " << M(2,2) << "\n";
  std::cout << "Writing " << t << " to M(2,2)\n";
  M.store(2,2,t);
  std::cout << "Reading at M(2,2): " << M(2,2) << "\n";
  std::cout << "Matrix M:\n" << M;

  ml_cuda::matrix_t<double> S(4, 4, [](ml_cuda::index_t row, ml_cuda::index_t col){return 2.0+0*row*col;}, true);
  std::cout << "Matrix S:\n" << S;

  ml_cuda::matrix_t<double> C_values(4, 4, [](ml_cuda::index_t row, ml_cuda::index_t col){return 0*row+0*col;}, false);
  ml_cuda::matrix_t<double> C_tangents1(4, 4, [](ml_cuda::index_t row, ml_cuda::index_t col){return 0*row+0*col;}, false);
  ml_cuda::matrix_t<double> C_tangents2(4, 4, [](ml_cuda::index_t row, ml_cuda::index_t col){return 0*row+0*col;}, false);

  ml_cuda::matrix_t<double> C_tangents[] = {C_tangents1,C_tangents2};
  ml_cuda::separate_tangent_matrix_t<double, 2> C(C_values, C_tangents);

  S.copyDataToDevice();
  M.copyDataToDevice();
  C.copyDataToDevice();

  std::cout << "Matrix C:\n" << C;
  std::cout << "Calling matmul(S,M,C):\n";

  cudaError_t error = cudaGetLastError();
  if(error!=0) std::cout << "last Cuda error before kernel=" << error << ", reason: " << cudaGetErrorString(error) << "\n";

  ml_cuda::matmul_reference<<<2,2>>>(S,M,C);
  cudaDeviceSynchronize();

  std::cout << "Matrix C (before copyDataToHost):\n" << C;
  C.copyDataToHost();
  std::cout << "Matrix C:\n" << C;

  error = cudaGetLastError();
  if(error!=0) std::cout << "last Cuda error after kernel=" << error << ", reason: " << cudaGetErrorString(error) << "\n";

  return 0;
}
