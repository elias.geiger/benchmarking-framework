#include "ml_cuda/ml_cuda.hpp"
#include <iostream>
#include <limits>

int main() {

  ml_cuda::matrix_t<float> f(4, 4, [](index_t row, index_t col){return ml_cuda::init_random<float>(row, col);});
  std::cout << "\nmatrix_t<float>:\n" << f;

  ml_cuda::matrix_t<double> d(4, 4, [](index_t row, index_t col){return ml_cuda::init_random<double>(row, col);});
  std::cout << "\nmatrix_t<double>:\n" << d;

  ml_cuda::matrix_t<ml_cuda::interval_t<float>> i_f(4, 4, [](index_t row, index_t col){return ml_cuda::init_random<ml_cuda::interval_t<float>>(row, col);});
  std::cout << "\nmatrix_t<ml_cuda::interval_t<float>>:\n" << i_f;

  ml_cuda::matrix_t<ml_cuda::interval_t<double>> i_d(4, 4, [](index_t row, index_t col){return ml_cuda::init_random<ml_cuda::interval_t<double>>(row, col);});
  std::cout << "\nmatrix_t<ml_cuda::interval_t<double>>:\n" << i_d;

  ml_cuda::matrix_t<ml_cuda::tangent_t<float>> t_f(4, 4, [](index_t row, index_t col){return ml_cuda::init_random<ml_cuda::tangent_t<float>>(row, col);});
  std::cout << "\nmatrix_t<ml_cuda::tangent_t<float>>:\n" << t_f;

  ml_cuda::matrix_t<ml_cuda::tangent_t<double>> t_d(4, 4, [](index_t row, index_t col){return ml_cuda::init_random<ml_cuda::tangent_t<double>>(row, col);});
  std::cout << "\nmatrix_t<ml_cuda::tangent_t<double>>:\n" << t_d;

  ml_cuda::matrix_t<ml_cuda::interval_t<ml_cuda::tangent_t<float>>> i_t_f(4, 4, [](index_t row, index_t col){return ml_cuda::init_random<ml_cuda::interval_t<ml_cuda::tangent_t<float>>>(row, col);});
  std::cout << "\nmatrix_t<ml_cuda::interval_t<ml_cuda::tangent_t<float>>>:\n" << i_t_f;

  ml_cuda::matrix_t<ml_cuda::interval_t<ml_cuda::tangent_t<double>>> i_t_d(4, 4, [](index_t row, index_t col){return ml_cuda::init_random<ml_cuda::interval_t<ml_cuda::tangent_t<double>>>(row, col);});
  std::cout << "\nmatrix_t<ml_cuda::interval_t<ml_cuda::tangent_t<double>>>:\n" << i_t_d;

  ml_cuda::matrix_t<ml_cuda::tangent_t<float,4>> t_f_4(4, 4, [](index_t row, index_t col){return ml_cuda::init_random<ml_cuda::tangent_t<float,4>>(row, col);});
  std::cout << "\nmatrix_t<ml_cuda::tangent_t<float,4>>:\n" << t_f_4;

  ml_cuda::matrix_t<ml_cuda::tangent_t<float,16>> t_f_16(4, 4, [](index_t row, index_t col){return ml_cuda::init_random<ml_cuda::tangent_t<float,16>>(row, col);});
  std::cout << "\nmatrix_t<ml_cuda::tangent_t<float,16>>:\n" << t_f_16;

  ml_cuda::matrix_t<ml_cuda::tangent_t<float,64>> t_f_64(4, 4, [](index_t row, index_t col){return ml_cuda::init_random<ml_cuda::tangent_t<float,64>>(row, col);});
  std::cout << "\nmatrix_t<ml_cuda::tangent_t<float,64>>:\n" << t_f_64;

  ml_cuda::matrix_t<ml_cuda::tangent_t<double,4>> t_d_4(4, 4, [](index_t row, index_t col){return ml_cuda::init_random<ml_cuda::tangent_t<double,4>>(row, col);});
  std::cout << "\nmatrix_t<ml_cuda::tangent_t<double,4>>:\n" << t_d_4;

  ml_cuda::matrix_t<ml_cuda::tangent_t<double,16>> t_d_16(4, 4, [](index_t row, index_t col){return ml_cuda::init_random<ml_cuda::tangent_t<double,16>>(row, col);});
  std::cout << "\nmatrix_t<ml_cuda::tangent_t<double,16>>:\n" << t_d_16;

  ml_cuda::matrix_t<ml_cuda::tangent_t<double,64>> t_d_64(4, 4, [](index_t row, index_t col){return ml_cuda::init_random<ml_cuda::tangent_t<double,64>>(row, col);});
  std::cout << "\nmatrix_t<ml_cuda::tangent_t<double,64>>:\n" << t_d_64;

  ml_cuda::matrix_t<ml_cuda::interval_t<ml_cuda::tangent_t<float,4>>> i_t_f_4(4, 4, [](index_t row, index_t col){return ml_cuda::init_random<ml_cuda::interval_t<ml_cuda::tangent_t<float,4>>>(row, col);});
  std::cout << "\nmatrix_t<ml_cuda::interval_t<ml_cuda::tangent_t<float,4>>>:\n" << i_t_f_4;

  ml_cuda::matrix_t<ml_cuda::interval_t<ml_cuda::tangent_t<float,16>>> i_t_f_16(4, 4, [](index_t row, index_t col){return ml_cuda::init_random<ml_cuda::interval_t<ml_cuda::tangent_t<float,16>>>(row, col);});
  std::cout << "\nmatrix_t<ml_cuda::interval_t<ml_cuda::tangent_t<float,16>>>:\n" << i_t_f_16;

  ml_cuda::matrix_t<ml_cuda::interval_t<ml_cuda::tangent_t<float,64>>> i_t_f_64(4, 4, [](index_t row, index_t col){return ml_cuda::init_random<ml_cuda::interval_t<ml_cuda::tangent_t<float,64>>>(row, col);});
  std::cout << "\nmatrix_t<ml_cuda::interval_t<ml_cuda::tangent_t<float,64>>>:\n" << i_t_f_64;

  ml_cuda::matrix_t<ml_cuda::interval_t<ml_cuda::tangent_t<double,4>>> i_t_d_4(4, 4, [](index_t row, index_t col){return ml_cuda::init_random<ml_cuda::interval_t<ml_cuda::tangent_t<double,4>>>(row, col);});
  std::cout << "\nmatrix_t<ml_cuda::interval_t<ml_cuda::tangent_t<double,4>>>:\n" << i_t_d_4;

  ml_cuda::matrix_t<ml_cuda::interval_t<ml_cuda::tangent_t<double,16>>> i_t_d_16(4, 4, [](index_t row, index_t col){return ml_cuda::init_random<ml_cuda::interval_t<ml_cuda::tangent_t<double,16>>>(row, col);});
  std::cout << "\nmatrix_t<ml_cuda::interval_t<ml_cuda::tangent_t<double,16>>>:\n" << i_t_d_16;

  ml_cuda::matrix_t<ml_cuda::interval_t<ml_cuda::tangent_t<double,64>>> i_t_d_64(4, 4, [](index_t row, index_t col){return ml_cuda::init_random<ml_cuda::interval_t<ml_cuda::tangent_t<double,64>>>(row, col);});
  std::cout << "\nmatrix_t<ml_cuda::interval_t<ml_cuda::tangent_t<double,64>>>:\n" << i_t_d_64;
}
