#include <iostream>
#include <cmath>

#define M 128
#define K 1024
#define N 512

#define grid_size_x 2
#define grid_size_y 2

#define block_size_x 17
#define block_size_y 16

#if  block_size_x < block_size_y
  #define k_step block_size_x
#else
  #define k_step block_size_y
#endif

void check(int,int,int,int);

int main() {
  for (int bx = 1; bx <= 64; ++bx) {
    for (int by = 1; by <= 64; ++by) {
      check((int)std::ceil((double)M/bx), (int)std::ceil((double)N/by), bx, by);
    }
  }
}

void check(int _grid_size_x,int _grid_size_y,
           int _block_size_x,int _block_size_y) {
  int _k_step = (_block_size_x<_block_size_y)?_block_size_x:_block_size_y;

  int i=0;
  while (i<K) { i += _k_step; }
  i -= _k_step;
  int lastAccessAlignmentA = i;
  i=0;
  while (i<K) { i += _k_step; }
  i -= _k_step;
  int lastAccessAlignmentB = i;

  bool bounds_check = true;
  if (_grid_size_x*_block_size_x==M &&
      _grid_size_y*_block_size_y==N &&
      lastAccessAlignmentA+_block_size_y==K &&
      lastAccessAlignmentB+_block_size_x==K) {
    bounds_check = false;
  }
  if (!bounds_check) {
    std::cout << "grid_size_x = : " << _grid_size_x << "\n"
                 "grid_size_y = : " << _grid_size_y << "\n"
                 "_block_size_x = : " << _block_size_x << "\n"
                 "_block_size_y = : " << _block_size_y << "\n"
                 "lastAccessAlignmentA: " << lastAccessAlignmentA << "\n"
                 "lastAccessAlignmentA: " << lastAccessAlignmentA << "\n"
                 "lastAccessAlignmentB: " << lastAccessAlignmentB << "\n"
                 "Bounds check needed: " << bounds_check << "\n";
  }
}

