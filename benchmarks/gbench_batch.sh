#!/usr/local_rwth/bin/zsh
 
# ask for 10 GB memory
#SBATCH --mem-per-cpu=10240M
 
# name the job
#SBATCH --job-name=ML_CUDA_GBENCH
 
# declare the merged STDOUT/STDERR file
#SBATCH --output=log_ML_CUDA_GBENCH_%J.txt

# request one pascal gpu
#SBATCH --gres=gpu:pascal:1

# request 3 hours wallclock
#SBATCH --time=0-03:00:00

### begin of executable commands

echo "[Batch Job] loading modules..."
module load DEVELOP
module unload intel
module unload gcc
module load gcc/7
module unload python
module load python/3.6.0
module unload cuda
module load cuda/102

echo "[Batch Job] starting benchmark  ..."
../bin/benchmark --benchmark_counters_tabular=true\
                 --benchmark_out=statistics_gbench.json\
                 --benchmark_out_format=json
