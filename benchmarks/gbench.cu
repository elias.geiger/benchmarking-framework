#include <iostream>
#include <benchmark/benchmark.h>
#include <chrono>
#include <sstream>

#include "ml_cuda/ml_cuda.hpp"
#include "kernels/tuned/matmul_16_6_c.cuh"
#include "kernels/tuned/matmul_18_16_c.cuh"
#include "kernels/tuned/matmul_20_8_c.cuh"
#include "kernels/tuned/matmul_32_7_c.cuh"
#include "kernels/tuned/matmul_32_7_c_ptr.cuh"
#include "kernels/tuned/matmul_48_10_c.cuh"
#include "kernels/tuned/matmul_48_10_c_ptr.cuh"
#include "kernels/tuned/matmul_4_8_c.cuh"
#include "kernels/tuned/matmul_5_4_c.cuh"
#include "kernels/tuned/matmul_5_4_c_ptr.cuh"
#include "kernels/tuned/matmul_5_6_c.cuh"
#include "kernels/tuned/matmul_5_6_c_ptr.cuh"
#include "kernels/tuned/matmul_64_5_c.cuh"
#include "kernels/tuned/matmul_64_5_c_ptr.cuh"
#include "kernels/tuned/matmul_8_12_c.cuh"
#include "kernels/tuned/matmul_8_12_c_ptr.cuh"
#include "kernels/tuned/matmul_8_8_n.cuh"
#include "kernels/tuned/matmul_8_8_n_ptr.cuh"


///-------------------------///
///   configure benchmark   ///
///-------------------------///
#define A_ROWS 128
#define A_COLS 1024
#define B_ROWS 1024
#define B_COLS 512

#define TRANSPOSE_A false
#define UNIFIED_MEM true

#define ITERATIONS 50
///-------------------------///

#if TRANSPOSE_A
  #define MAT_SIZE_M A_COLS
#else
  #define MAT_SIZE_M A_ROWS
#endif
#define MAT_SIZE_K B_ROWS
#define MAT_SIZE_N B_COLS

cudaEvent_t start_matInit;
cudaEvent_t stop_matInit;
cudaEvent_t start_runtime;
cudaEvent_t stop_runtime;
cudaEvent_t start_matCopyToDevice;
cudaEvent_t stop_matCopyToDevice;
cudaEvent_t start_kernelExec;
cudaEvent_t stop_kernelExec;
cudaEvent_t start_matCopyToHost;
cudaEvent_t stop_matCopyToHost;

enum dimension {
  D1 = 1,
  D2 = 2,
  D3 = 3
};

template<typename MAT_TYPE_A, typename MAT_TYPE_B, typename KERNEL_FUNC>
static inline void run_kernel(MAT_TYPE_A &A, MAT_TYPE_B &B, MAT_TYPE_B &C,
                              dim3 bSize, KERNEL_FUNC kernel,
                              dimension d, benchmark::State &state) {
  cudaError_t err;
  size_t iterations = 0;
  double sum_runtime = 0, sum_matCopyToDevice = 0,
      sum_kernelExec = 0, sum_matCopyToHost = 0;

  index_t gx = (index_t)std::ceil((double)MAT_SIZE_M/bSize.x);
  index_t gy = (index_t)std::ceil((double)MAT_SIZE_N/bSize.y);
  dim3 gSize(gx,gy);

  if (d == D1) {
    bSize.x = bSize.x*bSize.y*bSize.z;
    bSize.y = 1;
    bSize.z = 1;
    gSize.x = gSize.x*gSize.y*gSize.z;
    gSize.y = 1;
    gSize.z = 1;
  } else if (d == D2) {
    bSize.y = bSize.y*bSize.z;
    bSize.z = 1;
    gSize.y = gSize.y*gSize.z;
    gSize.z = 1;
  }

  auto start_total = std::chrono::high_resolution_clock::now();

  for(auto _ : state) {
    {
      cudaEventRecord(start_runtime);
      cudaEventRecord(start_matCopyToDevice);
      if(!UNIFIED_MEM) {
        A.copyDataToDevice();
        B.copyDataToDevice();
        C.copyDataToDevice();
      }
      cudaEventRecord(stop_matCopyToDevice);

      cudaDeviceSetSharedMemConfig(cudaSharedMemBankSizeEightByte);
      cudaDeviceSetCacheConfig(cudaFuncCachePreferShared);

      err = cudaGetLastError();
      if(err!=0) std::cerr << "Cuda error " << err
                           << " before kernel execution: "
                           << cudaGetErrorString(err) << "\n";

      cudaEventRecord(start_kernelExec);
      kernel<<<gSize,bSize>>>(A, B, C, TRANSPOSE_A);
      cudaEventRecord(stop_kernelExec);

      cudaDeviceSynchronize();
      err = cudaGetLastError();
      if(err!=0) std::cerr << "Cuda error " << err
                           << " after kernel execution: "
                           << cudaGetErrorString(err) << "\n";

      cudaEventRecord(start_matCopyToHost);
      if(!UNIFIED_MEM) {
        C.copyDataToHost();
      }
      cudaEventRecord(stop_matCopyToHost);
      cudaEventRecord(stop_runtime);

      cudaEventSynchronize(stop_runtime);
      cudaEventSynchronize(stop_matCopyToDevice);
      cudaEventSynchronize(stop_kernelExec);
      cudaEventSynchronize(stop_matCopyToHost);

      float ms_runtime = 0, ms_matCopyToDevice = 0,
          ms_kernelExec = 0, ms_matCopyToHost = 0;

      cudaEventElapsedTime(&ms_runtime,
                           start_runtime, stop_runtime);
      cudaEventElapsedTime(&ms_matCopyToDevice,
                           start_matCopyToDevice, stop_matCopyToDevice);
      cudaEventElapsedTime(&ms_kernelExec,
                           start_kernelExec, stop_kernelExec);
      cudaEventElapsedTime(&ms_matCopyToHost,
                           start_matCopyToHost, stop_matCopyToHost);

      sum_runtime += ms_runtime;
      sum_matCopyToDevice += ms_matCopyToDevice;
      sum_kernelExec += ms_kernelExec;
      sum_matCopyToHost += ms_matCopyToHost;

      state.SetIterationTime(ms_runtime);
      iterations++;
    }
  }

  if (iterations > 0) {
    state.counters["cp2d"] = sum_matCopyToDevice/iterations;
    state.counters["kernel"] = sum_kernelExec/iterations;
    state.counters["cp2h"] = sum_matCopyToHost/iterations;
    state.counters["run"] = sum_runtime/iterations;
  }

  auto stop_total = std::chrono::high_resolution_clock::now();
  auto elapsed_seconds =
      std::chrono::duration_cast<std::chrono::duration<double>>(stop_total -
                                                                start_total);
  state.counters["total"] = elapsed_seconds.count();
}

template<typename MAT_TYPE_A, typename MAT_TYPE_B, typename KERNEL_FUNC>
static inline void run_kernel_ptr(MAT_TYPE_A &A, MAT_TYPE_B &B, MAT_TYPE_B &C,
                                  dim3 bSize, KERNEL_FUNC kernel,
                                  dimension d, benchmark::State &state) {
  cudaError_t err;
  size_t iterations = 0;
  double sum_runtime = 0, sum_matCopyToDevice = 0,
      sum_kernelExec = 0, sum_matCopyToHost = 0;

  index_t gx = (index_t)std::ceil((double)MAT_SIZE_M/bSize.x);
  index_t gy = (index_t)std::ceil((double)MAT_SIZE_N/bSize.y);
  dim3 gSize(gx,gy);

  if (d == D1) {
    bSize.x = bSize.x*bSize.y*bSize.z;
    bSize.y = 1;
    bSize.z = 1;
    gSize.x = gSize.x*gSize.y*gSize.z;
    gSize.y = 1;
    gSize.z = 1;
  } else if (d == D2) {
    bSize.y = bSize.y*bSize.z;
    bSize.z = 1;
    gSize.y = gSize.y*gSize.z;
    gSize.z = 1;
  }

  auto start_total = std::chrono::high_resolution_clock::now();

  for(auto _ : state) {
    {
      cudaEventRecord(start_runtime);
      cudaEventRecord(start_matCopyToDevice);
      if(!UNIFIED_MEM) {
        A.copyDataToDevice();
        B.copyDataToDevice();
        C.copyDataToDevice();
      }
      A.copyInstanceToDevice();
      B.copyInstanceToDevice();
      C.copyInstanceToDevice();
      cudaEventRecord(stop_matCopyToDevice);

      cudaDeviceSetSharedMemConfig(cudaSharedMemBankSizeEightByte);
      cudaDeviceSetCacheConfig(cudaFuncCachePreferShared);

      err = cudaGetLastError();
      if(err!=0) std::cerr << "Cuda error " << err
                           << " before kernel execution: "
                           << cudaGetErrorString(err) << "\n";

      cudaEventRecord(start_kernelExec);
      kernel<<<gSize,bSize>>>(A.getDeviceInstance(), B.getDeviceInstance(),
                              C.getDeviceInstance(), TRANSPOSE_A);
      cudaEventRecord(stop_kernelExec);

      cudaDeviceSynchronize();
      err = cudaGetLastError();
      if(err!=0) std::cerr << "Cuda error " << err
                           << " after kernel execution: "
                           << cudaGetErrorString(err) << "\n";

      cudaEventRecord(start_matCopyToHost);
      if(!UNIFIED_MEM) {
        C.copyDataToHost();
      }
      A.deleteInstanceFromDevice();
      B.deleteInstanceFromDevice();
      C.deleteInstanceFromDevice();
      cudaEventRecord(stop_matCopyToHost);
      cudaEventRecord(stop_runtime);

      cudaEventSynchronize(stop_runtime);
      cudaEventSynchronize(stop_matCopyToDevice);
      cudaEventSynchronize(stop_kernelExec);
      cudaEventSynchronize(stop_matCopyToHost);

      float ms_runtime = 0, ms_matCopyToDevice = 0,
          ms_kernelExec = 0, ms_matCopyToHost = 0;

      cudaEventElapsedTime(&ms_runtime,
                           start_runtime, stop_runtime);
      cudaEventElapsedTime(&ms_matCopyToDevice,
                           start_matCopyToDevice, stop_matCopyToDevice);
      cudaEventElapsedTime(&ms_kernelExec,
                           start_kernelExec, stop_kernelExec);
      cudaEventElapsedTime(&ms_matCopyToHost,
                           start_matCopyToHost, stop_matCopyToHost);

      sum_runtime += ms_runtime;
      sum_matCopyToDevice += ms_matCopyToDevice;
      sum_kernelExec += ms_kernelExec;
      sum_matCopyToHost += ms_matCopyToHost;

      state.SetIterationTime(ms_runtime);
      iterations++;
    }
  }

  if (iterations > 0) {
    state.counters["cp2d"] = sum_matCopyToDevice/iterations;
    state.counters["kernel"] = sum_kernelExec/iterations;
    state.counters["cp2h"] = sum_matCopyToHost/iterations;
    state.counters["run"] = sum_runtime/iterations;
  }

  auto stop_total = std::chrono::high_resolution_clock::now();
  auto elapsed_seconds =
      std::chrono::duration_cast<std::chrono::duration<double>>(stop_total -
                                                                start_total);
  state.counters["total"] = elapsed_seconds.count();
}


template<typename TYPE_A, typename TYPE_B,
         void kernel(const ml_cuda::matrix_t<TYPE_A>,
                   const ml_cuda::matrix_t<TYPE_B>,
                   ml_cuda::matrix_t<TYPE_B>, bool),
         dimension d, index_t bSizeX=1, index_t bSizeY=1, index_t bSizeZ=1>
static void BM_MATMUL(benchmark::State& state) {

  cudaEventCreate(&start_matInit);
  cudaEventCreate(&stop_matInit);
  cudaEventCreate(&start_runtime);
  cudaEventCreate(&stop_runtime);
  cudaEventCreate(&start_matCopyToDevice);
  cudaEventCreate(&stop_matCopyToDevice);
  cudaEventCreate(&start_kernelExec);
  cudaEventCreate(&stop_kernelExec);
  cudaEventCreate(&start_matCopyToHost);
  cudaEventCreate(&stop_matCopyToHost);

  cudaEventRecord(start_matInit);
  ml_cuda::matrix_t<TYPE_A> A(A_ROWS, A_COLS, [](size_t row, size_t col) {
    return ml_cuda::init_random<TYPE_A>(row,col); }, UNIFIED_MEM);
  ml_cuda::matrix_t<TYPE_B> B(B_ROWS, B_COLS, [](size_t row, size_t col) {
    return ml_cuda::init_random<TYPE_B>(row,col); }, UNIFIED_MEM);
  ml_cuda::matrix_t<TYPE_B> C(MAT_SIZE_M, MAT_SIZE_N, UNIFIED_MEM);
  cudaEventRecord(stop_matInit);
  cudaEventSynchronize(stop_matInit);

  float ms_matInit = 0;
  cudaEventElapsedTime(&ms_matInit, start_matInit, stop_matInit);
  state.counters["init"] = ms_matInit;

  dim3 bSize(bSizeX, bSizeY, bSizeZ);
  run_kernel(A, B, C, bSize, kernel, d, state);

  cudaEventDestroy(start_matInit);
  cudaEventDestroy(stop_matInit);
  cudaEventDestroy(start_runtime);
  cudaEventDestroy(stop_runtime);
  cudaEventDestroy(start_matCopyToDevice);
  cudaEventDestroy(stop_matCopyToDevice);
  cudaEventDestroy(start_kernelExec);
  cudaEventDestroy(stop_kernelExec);
  cudaEventDestroy(start_matCopyToHost);
  cudaEventDestroy(stop_matCopyToHost);
}

template<typename TYPE_A, size_t TVS_A,
         typename TYPE_B, size_t TVS_B = TVS_A,
         void kernel(const ml_cuda::separate_tangent_matrix_t<TYPE_A, TVS_A>*,
                     const ml_cuda::separate_tangent_matrix_t<TYPE_B, TVS_B>*,
                     ml_cuda::separate_tangent_matrix_t<TYPE_B, TVS_B>*, bool),
         dimension d, index_t bSizeX=1, index_t bSizeY=1, index_t bSizeZ=1>
static void BM_MATMUL_SEPARATE(benchmark::State& state) {

  cudaEventCreate(&start_matInit);
  cudaEventCreate(&stop_matInit);
  cudaEventCreate(&start_runtime);
  cudaEventCreate(&stop_runtime);
  cudaEventCreate(&start_matCopyToDevice);
  cudaEventCreate(&stop_matCopyToDevice);
  cudaEventCreate(&start_kernelExec);
  cudaEventCreate(&stop_kernelExec);
  cudaEventCreate(&start_matCopyToHost);
  cudaEventCreate(&stop_matCopyToHost);

  cudaEventRecord(start_matInit);

  typedef ml_cuda::matrix_t<TYPE_A> primMatA_t;
  typedef ml_cuda::matrix_t<TYPE_B> primMatB_t;
  typedef ml_cuda::matrix_t<TYPE_A> tanMatA_t;
  typedef ml_cuda::matrix_t<TYPE_B> tanMatB_t;
  typedef ml_cuda::separate_tangent_matrix_t<TYPE_A, TVS_A> matA_t;
  typedef ml_cuda::separate_tangent_matrix_t<TYPE_B, TVS_B> matB_t;

  tanMatA_t A_tangents[TVS_A];
  tanMatB_t B_tangents[TVS_B];
  tanMatB_t C_tangents[TVS_B];

  primMatA_t A_values(A_ROWS, A_COLS, [](size_t row, size_t col) {
      return ml_cuda::init_random<TYPE_A>(row,col);}, UNIFIED_MEM);
  primMatB_t B_values(B_ROWS, B_COLS, [](size_t row, size_t col) {
      return ml_cuda::init_random<TYPE_B>(row,col);}, UNIFIED_MEM);
  primMatB_t C_values(MAT_SIZE_M, MAT_SIZE_N, UNIFIED_MEM);

  for (unsigned int i=0; i<TVS_A; ++i) {
    tanMatA_t tmp(A_ROWS, A_COLS, [](size_t row, size_t col) {
        return ml_cuda::init_random<TYPE_A>(row,col);}, UNIFIED_MEM);
    A_tangents[i] = tmp;
    A_tangents[i]._is_copy = false;
    tmp._is_copy = true;
  }
  for (unsigned int i=0; i<TVS_B; ++i) {
    tanMatB_t tmp1(B_ROWS, B_COLS, [](size_t row, size_t col) {
        return ml_cuda::init_random<TYPE_B>(row,col);}, UNIFIED_MEM);
    B_tangents[i] = tmp1;
    B_tangents[i]._is_copy = false;
    tmp1._is_copy = true;
    tanMatB_t tmp2(MAT_SIZE_M, MAT_SIZE_N, UNIFIED_MEM);
    C_tangents[i] = tmp2;
    C_tangents[i]._is_copy = false;
    tmp2._is_copy = true;
  }

  matA_t A(A_values, A_tangents);
  matB_t B(B_values, B_tangents);
  matB_t C(C_values, C_tangents);

  cudaEventRecord(stop_matInit);
  cudaEventSynchronize(stop_matInit);

  float ms_matInit = 0;
  cudaEventElapsedTime(&ms_matInit, start_matInit, stop_matInit);
  state.counters["init"] = ms_matInit;

  dim3 bSize(bSizeX, bSizeY, bSizeZ);
  run_kernel_ptr(A, B, C, bSize, kernel, d, state);

  cudaEventDestroy(start_matInit);
  cudaEventDestroy(stop_matInit);
  cudaEventDestroy(start_runtime);
  cudaEventDestroy(stop_runtime);
  cudaEventDestroy(start_matCopyToDevice);
  cudaEventDestroy(stop_matCopyToDevice);
  cudaEventDestroy(start_kernelExec);
  cudaEventDestroy(stop_kernelExec);
  cudaEventDestroy(start_matCopyToHost);
  cudaEventDestroy(stop_matCopyToHost);
}

///--------------------------------------------------------------------------///
///                       register new benchmarks here                       ///
///--------------------------------------------------------------------------///
BENCHMARK_TEMPLATE(BM_MATMUL, float,
                   float,
                   &ml_cuda::matmul_reference, D1, 20, 8
                   )->Iterations(ITERATIONS)->UseManualTime();
BENCHMARK_TEMPLATE(BM_MATMUL, float,
                   float,
                   &kernels::matmul_20_8_c, D2, 20, 8
                   )->Iterations(ITERATIONS)->UseManualTime();
BENCHMARK_TEMPLATE(BM_MATMUL, double,
                   double,
                   &ml_cuda::matmul_reference, D1, 64, 5
                   )->Iterations(ITERATIONS)->UseManualTime();
BENCHMARK_TEMPLATE(BM_MATMUL, double,
                   double,
                   &kernels::matmul_64_5_c, D2, 64, 5
                   )->Iterations(ITERATIONS)->UseManualTime();
BENCHMARK_TEMPLATE(BM_MATMUL, float,
                   ml_cuda::interval_t<float>,
                   &ml_cuda::matmul_reference, D1, 20, 8
                   )->Iterations(ITERATIONS)->UseManualTime();
BENCHMARK_TEMPLATE(BM_MATMUL, float,
                   ml_cuda::interval_t<float>,
                   &kernels::matmul_20_8_c, D2, 20, 8
                   )->Iterations(ITERATIONS)->UseManualTime();
BENCHMARK_TEMPLATE(BM_MATMUL, double,
                   ml_cuda::interval_t<double>,
                   &ml_cuda::matmul_reference, D1, 64, 5
                   )->Iterations(ITERATIONS)->UseManualTime();
BENCHMARK_TEMPLATE(BM_MATMUL, double,
                   ml_cuda::interval_t<double>,
                   &kernels::matmul_64_5_c, D2, 64, 5
                   )->Iterations(ITERATIONS)->UseManualTime();
BENCHMARK_TEMPLATE(BM_MATMUL, ml_cuda::tangent_t<float>,
                   ml_cuda::tangent_t<float>,
                   &ml_cuda::matmul_reference, D1, 5, 4
                   )->Iterations(ITERATIONS)->UseManualTime();
BENCHMARK_TEMPLATE(BM_MATMUL, ml_cuda::tangent_t<float>,
                   ml_cuda::tangent_t<float>,
                   &kernels::matmul_5_4_c, D2, 5, 4
                   )->Iterations(ITERATIONS)->UseManualTime();
BENCHMARK_TEMPLATE(BM_MATMUL_SEPARATE, float, 1,
                   float, 1,
                   &kernels::matmul_5_4_c_ptr, D2, 5, 4
                   )->Iterations(ITERATIONS)->UseManualTime();
BENCHMARK_TEMPLATE(BM_MATMUL, ml_cuda::tangent_t<float,4>,
                   ml_cuda::tangent_t<float,4>,
                   &ml_cuda::matmul_reference, D1, 48, 10
                   )->Iterations(ITERATIONS)->UseManualTime();
BENCHMARK_TEMPLATE(BM_MATMUL, ml_cuda::tangent_t<float,4>,
                   ml_cuda::tangent_t<float,4>,
                   &kernels::matmul_48_10_c, D2, 48, 10
                   )->Iterations(ITERATIONS)->UseManualTime();
BENCHMARK_TEMPLATE(BM_MATMUL_SEPARATE, float, 4,
                   float, 4,
                   &kernels::matmul_48_10_c_ptr, D2, 48, 10
                   )->Iterations(ITERATIONS)->UseManualTime();
BENCHMARK_TEMPLATE(BM_MATMUL, ml_cuda::tangent_t<float,16>,
                   ml_cuda::tangent_t<float,16>,
                   &ml_cuda::matmul_reference, D1, 8, 12
                   )->Iterations(ITERATIONS)->UseManualTime();
BENCHMARK_TEMPLATE(BM_MATMUL, ml_cuda::tangent_t<float,16>,
                   ml_cuda::tangent_t<float,16>,
                   &kernels::matmul_8_12_c, D2, 8, 12
                   )->Iterations(ITERATIONS)->UseManualTime();
BENCHMARK_TEMPLATE(BM_MATMUL_SEPARATE, float, 16,
                   float, 16,
                   &kernels::matmul_8_12_c_ptr, D2, 8, 12
                   )->Iterations(ITERATIONS)->UseManualTime();
BENCHMARK_TEMPLATE(BM_MATMUL, ml_cuda::tangent_t<float,64>,
                   ml_cuda::tangent_t<float,64>,
                   &ml_cuda::matmul_reference, D1, 8, 8
                   )->Iterations(ITERATIONS)->UseManualTime();
BENCHMARK_TEMPLATE(BM_MATMUL, ml_cuda::tangent_t<float,64>,
                   ml_cuda::tangent_t<float,64>,
                   &kernels::matmul_8_8_n, D2, 8, 8
                   )->Iterations(ITERATIONS)->UseManualTime();
BENCHMARK_TEMPLATE(BM_MATMUL_SEPARATE, float, 64,
                   float, 64,
                   &kernels::matmul_8_8_n_ptr, D2, 8, 8
                   )->Iterations(ITERATIONS)->UseManualTime();
BENCHMARK_TEMPLATE(BM_MATMUL, ml_cuda::tangent_t<double>,
                   ml_cuda::tangent_t<double>,
                   &ml_cuda::matmul_reference, D1, 64, 5
                   )->Iterations(ITERATIONS)->UseManualTime();
BENCHMARK_TEMPLATE(BM_MATMUL, ml_cuda::tangent_t<double>,
                   ml_cuda::tangent_t<double>,
                   &kernels::matmul_64_5_c, D2, 64, 5
                   )->Iterations(ITERATIONS)->UseManualTime();
BENCHMARK_TEMPLATE(BM_MATMUL_SEPARATE, double, 1,
                   double, 1,
                   &kernels::matmul_64_5_c_ptr, D2, 64, 5
                   )->Iterations(ITERATIONS)->UseManualTime();
BENCHMARK_TEMPLATE(BM_MATMUL, ml_cuda::tangent_t<double,4>,
                   ml_cuda::tangent_t<double,4>,
                   &ml_cuda::matmul_reference, D1, 32, 7
                   )->Iterations(ITERATIONS)->UseManualTime();
BENCHMARK_TEMPLATE(BM_MATMUL, ml_cuda::tangent_t<double,4>,
                   ml_cuda::tangent_t<double,4>,
                   &kernels::matmul_32_7_c, D2, 32, 7
                   )->Iterations(ITERATIONS)->UseManualTime();
BENCHMARK_TEMPLATE(BM_MATMUL_SEPARATE, double, 4,
                   double, 4,
                   &kernels::matmul_32_7_c_ptr, D2, 32, 7
                   )->Iterations(ITERATIONS)->UseManualTime();
BENCHMARK_TEMPLATE(BM_MATMUL, ml_cuda::tangent_t<double,16>,
                   ml_cuda::tangent_t<double,16>,
                   &ml_cuda::matmul_reference, D1, 8, 12
                   )->Iterations(ITERATIONS)->UseManualTime();
BENCHMARK_TEMPLATE(BM_MATMUL, ml_cuda::tangent_t<double,16>,
                   ml_cuda::tangent_t<double,16>,
                   &kernels::matmul_8_12_c, D2, 8, 12
                   )->Iterations(ITERATIONS)->UseManualTime();
BENCHMARK_TEMPLATE(BM_MATMUL_SEPARATE, double, 16,
                   double, 16,
                   &kernels::matmul_8_12_c_ptr, D2, 8, 12
                   )->Iterations(ITERATIONS)->UseManualTime();
BENCHMARK_TEMPLATE(BM_MATMUL, ml_cuda::tangent_t<double,64>,
                   ml_cuda::tangent_t<double,64>,
                   &ml_cuda::matmul_reference, D1, 5, 6
                   )->Iterations(ITERATIONS)->UseManualTime();
BENCHMARK_TEMPLATE(BM_MATMUL, ml_cuda::tangent_t<double,64>,
                   ml_cuda::tangent_t<double,64>,
                   &kernels::matmul_5_6_c, D2, 5, 6
                   )->Iterations(ITERATIONS)->UseManualTime();
BENCHMARK_TEMPLATE(BM_MATMUL_SEPARATE, double, 64,
                   double, 64,
                   &kernels::matmul_5_6_c_ptr, D2, 5, 6
                   )->Iterations(ITERATIONS)->UseManualTime();
BENCHMARK_TEMPLATE(BM_MATMUL, ml_cuda::tangent_t<float>,
                   ml_cuda::interval_t<ml_cuda::tangent_t<float>>,
                   &ml_cuda::matmul_reference, D1, 5, 4
                   )->Iterations(ITERATIONS)->UseManualTime();
BENCHMARK_TEMPLATE(BM_MATMUL, ml_cuda::tangent_t<float>,
                   ml_cuda::interval_t<ml_cuda::tangent_t<float>>,
                   &kernels::matmul_5_4_c, D2, 5, 4
                   )->Iterations(ITERATIONS)->UseManualTime();
BENCHMARK_TEMPLATE(BM_MATMUL, ml_cuda::tangent_t<float,4>,
                   ml_cuda::interval_t<ml_cuda::tangent_t<float,4>>,
                   &ml_cuda::matmul_reference, D1, 16, 6
                   )->Iterations(ITERATIONS)->UseManualTime();
BENCHMARK_TEMPLATE(BM_MATMUL, ml_cuda::tangent_t<float,4>,
                   ml_cuda::interval_t<ml_cuda::tangent_t<float,4>>,
                   &kernels::matmul_16_6_c, D2, 16, 6
                   )->Iterations(ITERATIONS)->UseManualTime();
BENCHMARK_TEMPLATE(BM_MATMUL, ml_cuda::tangent_t<float,16>,
                   ml_cuda::interval_t<ml_cuda::tangent_t<float,16>>,
                   &ml_cuda::matmul_reference, D1, 8, 8
                   )->Iterations(ITERATIONS)->UseManualTime();
BENCHMARK_TEMPLATE(BM_MATMUL, ml_cuda::tangent_t<float,16>,
                   ml_cuda::interval_t<ml_cuda::tangent_t<float,16>>,
                   &kernels::matmul_8_8_n, D2, 8, 8
                   )->Iterations(ITERATIONS)->UseManualTime();
BENCHMARK_TEMPLATE(BM_MATMUL, ml_cuda::tangent_t<float,64>,
                   ml_cuda::interval_t<ml_cuda::tangent_t<float,64>>,
                   &ml_cuda::matmul_reference, D1, 4, 8
                   )->Iterations(ITERATIONS)->UseManualTime();
BENCHMARK_TEMPLATE(BM_MATMUL, ml_cuda::tangent_t<float,64>,
                   ml_cuda::interval_t<ml_cuda::tangent_t<float,64>>,
                   &kernels::matmul_4_8_c, D2, 4, 8
                   )->Iterations(ITERATIONS)->UseManualTime();
BENCHMARK_TEMPLATE(BM_MATMUL, ml_cuda::tangent_t<double>,
                   ml_cuda::interval_t<ml_cuda::tangent_t<double>>,
                   &ml_cuda::matmul_reference, D1, 5, 4
                   )->Iterations(ITERATIONS)->UseManualTime();
BENCHMARK_TEMPLATE(BM_MATMUL, ml_cuda::tangent_t<double>,
                   ml_cuda::interval_t<ml_cuda::tangent_t<double>>,
                   &kernels::matmul_5_4_c, D2, 5, 4
                   )->Iterations(ITERATIONS)->UseManualTime();
BENCHMARK_TEMPLATE(BM_MATMUL, ml_cuda::tangent_t<double,4>,
                   ml_cuda::interval_t<ml_cuda::tangent_t<double,4>>,
                   &ml_cuda::matmul_reference, D1, 32, 7
                   )->Iterations(ITERATIONS)->UseManualTime();
BENCHMARK_TEMPLATE(BM_MATMUL, ml_cuda::tangent_t<double,4>,
                   ml_cuda::interval_t<ml_cuda::tangent_t<double,4>>,
                   &kernels::matmul_32_7_c, D2, 32, 7
                   )->Iterations(ITERATIONS)->UseManualTime();
BENCHMARK_TEMPLATE(BM_MATMUL, ml_cuda::tangent_t<double,16>,
                   ml_cuda::interval_t<ml_cuda::tangent_t<double,16>>,
                   &ml_cuda::matmul_reference, D1, 8, 8
                   )->Iterations(ITERATIONS)->UseManualTime();
BENCHMARK_TEMPLATE(BM_MATMUL, ml_cuda::tangent_t<double,16>,
                   ml_cuda::interval_t<ml_cuda::tangent_t<double,16>>,
                   &kernels::matmul_8_8_n, D2, 8, 8
                   )->Iterations(ITERATIONS)->UseManualTime();
BENCHMARK_TEMPLATE(BM_MATMUL, ml_cuda::tangent_t<double,64>,
                   ml_cuda::interval_t<ml_cuda::tangent_t<double,64>>,
                   &ml_cuda::matmul_reference, D1, 5, 6
                   )->Iterations(ITERATIONS)->UseManualTime();
BENCHMARK_TEMPLATE(BM_MATMUL, ml_cuda::tangent_t<double,64>,
                   ml_cuda::interval_t<ml_cuda::tangent_t<double,64>>,
                   &kernels::matmul_5_6_c, D2, 5, 6
                   )->Iterations(ITERATIONS)->UseManualTime();
///--------------------------------------------------------------------------///


int main(int argc, char** argv) {

  std::cout << "Benchmark configuration:"
            << "\nA_ROWS        \t"    << A_ROWS
            << "\nA_COLS        \t"    << A_COLS
            << "\nB_ROWS        \t"    << B_ROWS
            << "\nB_COLS        \t"    << B_COLS
            << "\nTRANSPOSE_A   \t"    << TRANSPOSE_A
            << "\nUNIFIED_MEM   \t"    << UNIFIED_MEM
            << "\nITERATIONS    \t"    << ITERATIONS
            << "\n";

  ::benchmark::Initialize(&argc, argv);
  if (::benchmark::ReportUnrecognizedArguments(argc, argv)) return 1;
  ::benchmark::RunSpecifiedBenchmarks();
}
