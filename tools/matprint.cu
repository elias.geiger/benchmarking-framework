#include "ml_cuda/ml_cuda.hpp"

#include <sstream>
#include <iostream>
#include <string.h>

using ml_cuda::type_t;
using ml_cuda::index_t;
using ml_cuda::interval_t;
using ml_cuda::tangent_t;
using ml_cuda::matrix_t;

int main(int argc, char** argv) {

  unsigned int tangentVectorSize = 1;
  index_t dims[2] = {0,0};
  type_t type;

  switch (argc) {
    case 1: {
      std::cout << "usage:\t 1) matprint <file>\n";
      return 0;
    }
    case 2: {
      if (!ml_cuda::parseHeader(argv[1], dims[0], dims[1], type, tangentVectorSize)) {
        std::cerr << "Error: Input filenames not valid!\n";
        exit(1);
      }
      break;
    }
    default: {
      std::cerr << "Error: Wrong argument count!\n";
      return 1;
    }
  }

  if (type.primals[0] == ml_cuda::FLOAT) {
    matrix_t<float> M(argv[1], false);
    std::cout << M;
  }
  else if (type.primals[0] == ml_cuda::DOUBLE) {
    matrix_t<double> M(argv[1], false);
    std::cout << M;
  }
  else if (type.primals[0] == ml_cuda::INTERVAL &&
           type.primals[1] == ml_cuda::FLOAT) {
    matrix_t<interval_t<float>> M(argv[1], false);
    std::cout << M;
  }
  else if (type.primals[0] == ml_cuda::INTERVAL &&
           type.primals[1] == ml_cuda::DOUBLE) {
    matrix_t<interval_t<double>> M(argv[1], false);
    std::cout << M;
  }
  else if (type.primals[0] == ml_cuda::TANGENT &&
           type.primals[1] == ml_cuda::FLOAT) {
    if (tangentVectorSize == 1) {
      matrix_t<tangent_t<float,1>> M(argv[1], false);
      std::cout << M;
    }
    else if (tangentVectorSize == 4) {
      matrix_t<tangent_t<float,4>> M(argv[1], false);
      std::cout << M;
    }
    else if (tangentVectorSize == 16) {
      matrix_t<tangent_t<float,16>> M(argv[1], false);
      std::cout << M;
    }
    else if (tangentVectorSize == 64) {
      matrix_t<tangent_t<float,64>> M(argv[1], false);
      std::cout << M;
    }
    else {
      std::cerr << "ERROR: unsupported tangentVectorSize: " << tangentVectorSize << "\n";
      return 1;
    }
  }
  else if (type.primals[0] == ml_cuda::TANGENT &&
           type.primals[1] == ml_cuda::DOUBLE) {
    if (tangentVectorSize == 1) {
      matrix_t<tangent_t<double,1>> M(argv[1], false);
      std::cout << M;
    }
    else if (tangentVectorSize == 4) {
      matrix_t<tangent_t<double,4>> M(argv[1], false);
      std::cout << M;
    }
    else if (tangentVectorSize == 16) {
      matrix_t<tangent_t<double,16>> M(argv[1], false);
      std::cout << M;
    }
    else if (tangentVectorSize == 64) {
      matrix_t<tangent_t<double,64>> M(argv[1], false);
      std::cout << M;
    }
    else {
      std::cerr << "ERROR: unsupported tangentVectorSize: " << tangentVectorSize << "\n";
      return 1;
    }
  }
  else if (type.primals[0] == ml_cuda::INTERVAL &&
           type.primals[1] == ml_cuda::TANGENT &&
           type.primals[2] == ml_cuda::FLOAT) {
    if (tangentVectorSize == 1) {
      matrix_t<interval_t<tangent_t<float,1>>> M(argv[1], false);
      std::cout << M;
    }
    else if (tangentVectorSize == 4) {
      matrix_t<interval_t<tangent_t<float,4>>> M(argv[1], false);
      std::cout << M;
    }
    else if (tangentVectorSize == 16) {
      matrix_t<interval_t<tangent_t<float,16>>> M(argv[1], false);
      std::cout << M;
    }
    else if (tangentVectorSize == 64) {
      matrix_t<interval_t<tangent_t<float,64>>> M(argv[1], false);
      std::cout << M;
    }
    else {
      std::cerr << "ERROR: unsupported tangentVectorSize: " << tangentVectorSize << "\n";
      return 1;
    }
  }
  else if (type.primals[0] == ml_cuda::INTERVAL &&
           type.primals[1] == ml_cuda::TANGENT &&
           type.primals[2] == ml_cuda::DOUBLE) {
    if (tangentVectorSize == 1) {
      matrix_t<interval_t<tangent_t<double,1>>> M(argv[1], false);
      std::cout << M;
    }
    else if (tangentVectorSize == 4) {
      matrix_t<interval_t<tangent_t<double,4>>> M(argv[1], false);
      std::cout << M;
    }
    else if (tangentVectorSize == 16) {
      matrix_t<interval_t<tangent_t<double,16>>> M(argv[1], false);
      std::cout << M;
    }
    else if (tangentVectorSize == 64) {
      matrix_t<interval_t<tangent_t<double,64>>> M(argv[1], false);
      std::cout << M;
    }
    else {
      std::cerr << "ERROR: unsupported tangentVectorSize: " << tangentVectorSize << "\n";
      return 1;
    }
  }
  else {
    std::cerr << "ERROR: Type combination not supported\n";
    return 1;
  }
}
