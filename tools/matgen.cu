#include <sstream>
#include <iostream>
#include <string.h>
#include <time.h>

#include "ml_cuda/ml_cuda.hpp"

using ml_cuda::index_t;
using ml_cuda::type_t;
using ml_cuda::interval_t;
using ml_cuda::tangent_t;
using ml_cuda::matrix_t;

template<typename TREAL_A, typename TREAL_B>
void generateMatrixByMatmul(index_t* dims, const char* fname, char** argv, type_t &type, unsigned int tangentVectorSize=1) {
  const matrix_t<TREAL_A> A(argv[1], true); // get by filename argv[1]
  const matrix_t<TREAL_B> B(argv[2], true); // get by filename argv[2]
  matrix_t<TREAL_B> C(dims[0], dims[2], true);
  C.setType(type);
  ml_cuda::matmul_reference<<<256,256>>>(A, B, C);
  cudaDeviceSynchronize();
  C.writeToFile(fname, tangentVectorSize);
}

int main(int argc, char** argv) { 
  bool useMatmul = false;
  std::stringstream fname;
  index_t dims[3] = {0,0,0};
  type_t types[2];

  unsigned int tangentVectorSize = 1;

  switch (argc) {
    case 1: {
      std::cout << "usage:\t 1) matgen <rows> <cols> <type> optional:<tangentVectorSize>\n" << "\t 2) matgen <A_file> <B_file>\n";
      std::cout << "\n<type>:\t 1) f (float)\n\t 2) d (double)\n\t 3) i (interval)\n\t 3) t (tangent)\n";
      std::cout << "\n<tangentVectorSize>: 4, 16 or 64\n";
      return 0;
    }
    case 3: { /* generate by matmul */
      useMatmul = true;
      fname << argv[1];
      fname.seekp(fname.tellp() - (std::streampos)7); // remove .mlcuda file ext
      fname << "_" << argv[2];
      if (!ml_cuda::parseHeader(argv[1], dims[0], dims[1], types[0], tangentVectorSize) ||
          !ml_cuda::parseHeader(argv[2], dims[1], dims[2], types[1], tangentVectorSize)) {
        std::cerr << "Error: Input Files are not valid!\n";
        exit(1);
      }
      break;
    }
    case 5: {
      tangentVectorSize = (unsigned int)std::atoi(argv[4]);
    }
    case 4: { /* generate simple */
      dims[0] = (index_t)std::atoi(argv[1]);
      dims[1] = (index_t)std::atoi(argv[2]);
      ml_cuda::parseInputType(argv[3],types[0]);
      fname << dims[0] << '_' << dims[1] << '_' << types[0];
      if (tangentVectorSize>1) fname << tangentVectorSize;
      fname << ".mlcuda";
      break;
    }
    default: {
      std::cerr << "ERROR: Wrong argument count\n";
      return 1;
    }
  }

  if (useMatmul) {  /* generate by matmul */
    if (types[0].primals[0] == ml_cuda::FLOAT &&
        types[1].primals[0] == ml_cuda::FLOAT) {
      generateMatrixByMatmul<float, float> (dims, fname.str().c_str(), argv, types[1]);
    }
    else if (types[0].primals[0] == ml_cuda::DOUBLE &&
             types[1].primals[0] == ml_cuda::DOUBLE) {
      generateMatrixByMatmul<double, double> (dims, fname.str().c_str(), argv, types[1]);
    }
    else if (types[0].primals[0] == ml_cuda::FLOAT &&
             types[1].primals[0] == ml_cuda::INTERVAL &&
             types[1].primals[1] == ml_cuda::FLOAT) {
      generateMatrixByMatmul<float, interval_t<float>> (dims, fname.str().c_str(), argv, types[1]);
    }
    else if (types[0].primals[0] == ml_cuda::DOUBLE &&
             types[1].primals[0] == ml_cuda::INTERVAL &&
             types[1].primals[1] == ml_cuda::DOUBLE) {
      generateMatrixByMatmul<double, interval_t<double>> (dims, fname.str().c_str(), argv, types[1]);
    }
    else if (types[0].primals[0] == ml_cuda::TANGENT &&
             types[0].primals[1] == ml_cuda::FLOAT &&
             types[1].primals[0] == ml_cuda::TANGENT &&
             types[1].primals[1] == ml_cuda::FLOAT) {
      if (tangentVectorSize == 1) {
        generateMatrixByMatmul<tangent_t<float>, tangent_t<float>> (dims, fname.str().c_str(), argv, types[1]);
      }
      else if (tangentVectorSize == 4) {
        generateMatrixByMatmul<tangent_t<float,4>, tangent_t<float,4>> (dims, fname.str().c_str(), argv, types[1], 4);
      }
      else if (tangentVectorSize == 16) {
        generateMatrixByMatmul<tangent_t<float,16>, tangent_t<float,16>> (dims, fname.str().c_str(), argv, types[1], 16);
      }
      else if (tangentVectorSize == 64) {
        generateMatrixByMatmul<tangent_t<float,64>, tangent_t<float,64>> (dims, fname.str().c_str(), argv, types[1], 64);
      }
      else {
        std::cerr << "ERROR: unsupported tangentVectorSize: " << tangentVectorSize << "\n";
        return 1;
      }
    }
    else if (types[0].primals[0] == ml_cuda::TANGENT &&
             types[0].primals[1] == ml_cuda::DOUBLE &&
             types[1].primals[0] == ml_cuda::TANGENT &&
             types[1].primals[1] == ml_cuda::DOUBLE) {
      if (tangentVectorSize == 1) {
        generateMatrixByMatmul<tangent_t<double>, tangent_t<double>> (dims, fname.str().c_str(), argv, types[1]);
      }
      else if (tangentVectorSize == 4) {
        generateMatrixByMatmul<tangent_t<double,4>, tangent_t<double,4>> (dims, fname.str().c_str(), argv, types[1], 4);
      }
      else if (tangentVectorSize == 16) {
        generateMatrixByMatmul<tangent_t<double,16>, tangent_t<double,16>> (dims, fname.str().c_str(), argv, types[1], 16);
      }
      else if (tangentVectorSize == 64) {
        generateMatrixByMatmul<tangent_t<double,64>, tangent_t<double,64>> (dims, fname.str().c_str(), argv, types[1], 64);
      }
      else {
        std::cerr << "ERROR: unsupported tangentVectorSize: " << tangentVectorSize << "\n";
        return 1;
      }
    }
    else if (types[0].primals[0] == ml_cuda::TANGENT &&
             types[0].primals[1] == ml_cuda::FLOAT &&
             types[1].primals[0] == ml_cuda::INTERVAL &&
             types[1].primals[1] == ml_cuda::TANGENT &&
             types[1].primals[2] == ml_cuda::FLOAT) {
      if (tangentVectorSize == 1) {
        generateMatrixByMatmul<tangent_t<float>, interval_t<tangent_t<float>>> (dims, fname.str().c_str(), argv, types[1]);
      }
      else if (tangentVectorSize == 4) {
        generateMatrixByMatmul<tangent_t<float,4>, interval_t<tangent_t<float,4>>> (dims, fname.str().c_str(), argv, types[1], 4);
      }
      else if (tangentVectorSize == 16) {
        generateMatrixByMatmul<tangent_t<float,16>, interval_t<tangent_t<float,16>>> (dims, fname.str().c_str(), argv, types[1], 16);
      }
      else if (tangentVectorSize == 64) {
        generateMatrixByMatmul<tangent_t<float,64>, interval_t<tangent_t<float,64>>> (dims, fname.str().c_str(), argv, types[1], 64);
      }
      else {
        std::cerr << "ERROR: unsupported tangentVectorSize: " << tangentVectorSize << "\n";
        return 1;
      }
    }
    else if (types[0].primals[0] == ml_cuda::TANGENT &&
             types[0].primals[1] == ml_cuda::DOUBLE &&
             types[1].primals[0] == ml_cuda::INTERVAL &&
             types[1].primals[1] == ml_cuda::TANGENT &&
             types[1].primals[2] == ml_cuda::DOUBLE) {
      if (tangentVectorSize == 1) {
        generateMatrixByMatmul<tangent_t<double>, interval_t<tangent_t<double>>> (dims, fname.str().c_str(), argv, types[1]);
      }
      else if (tangentVectorSize == 4) {
        generateMatrixByMatmul<tangent_t<double,4>, interval_t<tangent_t<double,4>>> (dims, fname.str().c_str(), argv, types[1], 4);
      }
      else if (tangentVectorSize == 16) {
        generateMatrixByMatmul<tangent_t<double,16>, interval_t<tangent_t<double,16>>> (dims, fname.str().c_str(), argv, types[1], 16);
      }
      else if (tangentVectorSize == 64) {
        generateMatrixByMatmul<tangent_t<double,64>, interval_t<tangent_t<double,64>>> (dims, fname.str().c_str(), argv, types[1], 64);
      }
      else {
        std::cerr << "ERROR: unsupported tangentVectorSize: " << tangentVectorSize << "\n";
        return 1;
      }
    }
    else {
      std::cerr << "ERROR: Type combination not supported\n";
      return 1;
    }
  } else {  /* generate simple */
    if (types[0].primals[0] == ml_cuda::FLOAT) {
      matrix_t<float> M(dims[0], dims[1], [](index_t row, index_t col){
        return ml_cuda::init_random<float>(row,col);
      });
      M.setType(types[0]);
      M.writeToFile(fname.str().c_str());
    }
    else if (types[0].primals[0] == ml_cuda::DOUBLE) {
      matrix_t<double> M(dims[0], dims[1], [](index_t row, index_t col){
        return ml_cuda::init_random<double>(row,col);
      });
      M.setType(types[0]);
      M.writeToFile(fname.str().c_str());
    }
    else if (types[0].primals[0] == ml_cuda::INTERVAL &&
             types[0].primals[1] == ml_cuda::FLOAT) {
      matrix_t<interval_t<float>> M(dims[0], dims[1], [](index_t row, index_t col){
        return ml_cuda::init_random<interval_t<float>>(row,col);
      });
      M.setType(types[0]);
      M.writeToFile(fname.str().c_str());
    }
    else if (types[0].primals[0] == ml_cuda::INTERVAL &&
             types[0].primals[1] == ml_cuda::DOUBLE) {
      matrix_t<interval_t<double>> M(dims[0], dims[1], [](index_t row, index_t col){
        return ml_cuda::init_random<interval_t<double>>(row,col);
      });
      M.setType(types[0]);
      M.writeToFile(fname.str().c_str());
    }
    else if (types[0].primals[0] == ml_cuda::TANGENT &&
             types[0].primals[1] == ml_cuda::FLOAT) {
      if (tangentVectorSize == 1) {
        matrix_t<tangent_t<float,1>> M(dims[0], dims[1], [](index_t row, index_t col){
          return ml_cuda::init_random<tangent_t<float,1>>(row,col);
        });
        M.setType(types[0]);
        M.writeToFile(fname.str().c_str());
      }
      else if (tangentVectorSize == 4) {
        matrix_t<tangent_t<float,4>> M(dims[0], dims[1], [](index_t row, index_t col){
          return ml_cuda::init_random<tangent_t<float,4>>(row,col);
        });
        M.setType(types[0]);
        M.writeToFile(fname.str().c_str(), tangentVectorSize);
      }
      else if (tangentVectorSize == 16) {
        matrix_t<tangent_t<float,16>> M(dims[0], dims[1], [](index_t row, index_t col){
          return ml_cuda::init_random<tangent_t<float,16>>(row,col);
        });
        M.setType(types[0]);
        M.writeToFile(fname.str().c_str(), tangentVectorSize);
      }
      else if (tangentVectorSize == 64) {
        matrix_t<tangent_t<float,64>> M(dims[0], dims[1], [](index_t row, index_t col){
          return ml_cuda::init_random<tangent_t<float,64>>(row,col);
        });
        M.setType(types[0]);
        M.writeToFile(fname.str().c_str(), tangentVectorSize);
      }
      else {
        std::cerr << "ERROR: unsupported tangentVectorSize: " << tangentVectorSize << "\n";
        return 1;
      }
    }
    else if (types[0].primals[0] == ml_cuda::TANGENT &&
             types[0].primals[1] == ml_cuda::DOUBLE) {
      if (tangentVectorSize == 1) {
        matrix_t<tangent_t<double,1>> M(dims[0], dims[1], [](index_t row, index_t col){
          return ml_cuda::init_random<tangent_t<double,1>>(row,col);
        });
        M.setType(types[0]);
        M.writeToFile(fname.str().c_str());
      }
      else if (tangentVectorSize == 4) {
        matrix_t<tangent_t<double,4>> M(dims[0], dims[1], [](index_t row, index_t col){
          return ml_cuda::init_random<tangent_t<double,4>>(row,col);
        });
        M.setType(types[0]);
        M.writeToFile(fname.str().c_str(), tangentVectorSize);
      }
      else if (tangentVectorSize == 16) {
        matrix_t<tangent_t<double,16>> M(dims[0], dims[1], [](index_t row, index_t col){
          return ml_cuda::init_random<tangent_t<double,16>>(row,col);
        });
        M.setType(types[0]);
        M.writeToFile(fname.str().c_str(), tangentVectorSize);
      }
      else if (tangentVectorSize == 64) {
        matrix_t<tangent_t<double,64>> M(dims[0], dims[1], [](index_t row, index_t col){
          return ml_cuda::init_random<tangent_t<double,64>>(row,col);
        });
        M.setType(types[0]);
        M.writeToFile(fname.str().c_str(), tangentVectorSize);
      }
      else {
        std::cerr << "ERROR: unsupported tangentVectorSize: " << tangentVectorSize << "\n";
        return 1;
      }
    }
    else if (types[0].primals[0] == ml_cuda::INTERVAL &&
             types[0].primals[1] == ml_cuda::TANGENT &&
             types[0].primals[2] == ml_cuda::FLOAT) {
      if (tangentVectorSize == 1) {
        matrix_t<interval_t<tangent_t<float,1>>> M(dims[0], dims[1], [](index_t row, index_t col){
          return ml_cuda::init_random<interval_t<tangent_t<float,1>>>(row,col);
        });
        M.setType(types[0]);
        M.writeToFile(fname.str().c_str());
      }
      else if (tangentVectorSize == 4) {
        matrix_t<interval_t<tangent_t<float,4>>> M(dims[0], dims[1], [](index_t row, index_t col){
          return ml_cuda::init_random<interval_t<tangent_t<float,4>>>(row,col);
        });
        M.setType(types[0]);
        M.writeToFile(fname.str().c_str(), tangentVectorSize);
      }
      else if (tangentVectorSize == 16) {
        matrix_t<interval_t<tangent_t<float,16>>> M(dims[0], dims[1], [](index_t row, index_t col){
          return ml_cuda::init_random<interval_t<tangent_t<float,16>>>(row,col);
        });
        M.setType(types[0]);
        M.writeToFile(fname.str().c_str(), tangentVectorSize);
      }
      else if (tangentVectorSize == 64) {
        matrix_t<interval_t<tangent_t<float,64>>> M(dims[0], dims[1], [](index_t row, index_t col){
          return ml_cuda::init_random<interval_t<tangent_t<float,64>>>(row,col);
        });
        M.setType(types[0]);
        M.writeToFile(fname.str().c_str(), tangentVectorSize);
      }
      else {
        std::cerr << "ERROR: unsupported tangentVectorSize: " << tangentVectorSize << "\n";
        return 1;
      }
    }
    else if (types[0].primals[0] == ml_cuda::INTERVAL &&
             types[0].primals[1] == ml_cuda::TANGENT &&
             types[0].primals[2] == ml_cuda::DOUBLE) {
      if (tangentVectorSize == 1) {
        matrix_t<interval_t<tangent_t<double,1>>> M(dims[0], dims[1], [](index_t row, index_t col){
          return ml_cuda::init_random<interval_t<tangent_t<double,1>>>(row,col);
        });
        M.setType(types[0]);
        M.writeToFile(fname.str().c_str());
      }
      else if (tangentVectorSize == 4) {
        matrix_t<interval_t<tangent_t<double,4>>> M(dims[0], dims[1], [](index_t row, index_t col){
          return ml_cuda::init_random<interval_t<tangent_t<double,4>>>(row,col);
        });
        M.setType(types[0]);
        M.writeToFile(fname.str().c_str(), tangentVectorSize);
      }
      else if (tangentVectorSize == 16) {
        matrix_t<interval_t<tangent_t<double,16>>> M(dims[0], dims[1], [](index_t row, index_t col){
          return ml_cuda::init_random<interval_t<tangent_t<double,16>>>(row,col);
        });
        M.setType(types[0]);
        M.writeToFile(fname.str().c_str(), tangentVectorSize);
      }
      else if (tangentVectorSize == 64) {
        matrix_t<interval_t<tangent_t<double,64>>> M(dims[0], dims[1], [](index_t row, index_t col){
          return ml_cuda::init_random<interval_t<tangent_t<double,64>>>(row,col);
        });
        M.setType(types[0]);
        M.writeToFile(fname.str().c_str(), tangentVectorSize);
      }
      else {
        std::cerr << "ERROR: unsupported tangentVectorSize: " << tangentVectorSize << "\n";
        return 1;
      }
    }
    else {
      std::cerr << "ERROR: Type combination not supported\n";
      return 1;
    }
  }
}
