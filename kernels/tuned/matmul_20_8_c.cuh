#ifndef MATMUL_20_8_C_HPP
#define MATMUL_20_8_C_HPP

#include <type_traits>
#include <device_launch_parameters.h>

#include "../../ml_cuda/traits/ml_cuda_traits.hpp"
#include "../../ml_cuda/ml_cuda.hpp"

namespace kernels {

template<typename MATRIX_A, typename MATRIX_B, typename MATRIX_C,
         typename TI_A = ml_cuda::traits::type_info<MATRIX_A>,
         typename TI_B = ml_cuda::traits::type_info<MATRIX_B>,
         typename TI_C = ml_cuda::traits::type_info<MATRIX_C>,
         typename check_A = typename std::enable_if<TI_A::is_ml_cuda_matrix>::type,
         typename check_B = typename std::enable_if<TI_B::is_ml_cuda_matrix>::type,
         typename check_C = typename std::enable_if<TI_C::is_ml_cuda_matrix>::type>
__global__ void matmul_20_8_c(const MATRIX_A A, const MATRIX_B B, MATRIX_C C,
                       bool transposeA=false) {

  typedef typename std::remove_const<typename TI_A::real_t>::type TYPE_A;
  typedef typename std::remove_const<typename TI_B::real_t>::type TYPE_B;
  typedef typename TI_C::real_t TYPE_C;

  #define op_A_safe(r,c)  (transposeA?A.get_safe(c,r):A.get_safe(r,c))
  #define op_A(r,c)       (transposeA?A(c,r):A(r,c))

  const index_t K = B.rows();

  const index_t tX = threadIdx.x;
  const index_t tY = threadIdx.y;
  const index_t gX = blockIdx.x*blockDim.x+tX;
  const index_t gY = blockIdx.y*blockDim.y+tY;

  __shared__ TYPE_A sA[20][8];
  __shared__ TYPE_B sB[20][8];
  TYPE_C pSum = TYPE_C(0);

  for (index_t k_offset=0; k_offset<K; k_offset+=8) {
    sA[tX][tY] = op_A_safe(gX, k_offset+tY);
    sB[tX][tY] = B.get_safe(k_offset+tX, gY);
    __syncthreads();
    for (index_t i=0; i<8; ++i) {
        pSum += sA[tX][i]*sB[i][tY];
    }
    __syncthreads();
  }
  C.store_safe(gX, gY, pSum);
}

} // namespace kernels_staticShared

#endif // MATMUL_20_8_C_HPP
