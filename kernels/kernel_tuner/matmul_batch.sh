#!/usr/local_rwth/bin/zsh
 
# ask for 10 GB memory
#SBATCH --mem-per-cpu=10240M
 
# name the job
#SBATCH --job-name=ML_CUDA_TUNER
 
# declare the merged STDOUT/STDERR file
#SBATCH --output=log_ML_CUDA_TUNER_%J.txt

# request one pascal gpu
#SBATCH --gres=gpu:pascal:1

# request 3 days wallclock
#SBATCH --time=3-00:00:00

### begin of executable commands


# matrix dimension are encoded as "M K N"
declare -a supportedDimensions
supportedDimensions=("32 256 128"\
                     "64 512 256"\
                     "128 1024 512"\
                     "256 2048 1024")
# !Bug on ComputeCluster: use at least two entries

# matmul operation is encoded as "typeA typeB TVS"
  #--> types: "f(loat)"=1, "d(ouble)"=2, "i(nterval)"=3, "t(angent)"=4
declare -a supportedOperations
supportedOperations=("f f 1"\
                     "d d 1"\
                     "f if 1"\
                     "d id 1"\
                     "tf tf 1" "tf tf 4" "tf tf 16" "tf tf 64"\
                     "td td 1" "td td 4" "td td 16" "td td 64"\
                     "tf itf 1" "tf itf 4" "tf itf 16" "tf itf 64"\
                     "td itd 1" "td itd 4" "td itd 16" "td itd 64")
# !Bug on ComputeCluster: use at least two entries

# set the amount of indexing dimensions used by the benchmarked kernel
  #--> dimensions used by kernel: "x"="1", "x,y"="2", "x,y,z"="3"
indexDims="2"

# specify if the input matrices should be generated (or are provided ->false)
generateMatrices=true
removeGeneratedMatricesAfterwards=true

#----------------------------------------------------------------------------#

echo "Loading modules..."
module load DEVELOP
module unload intel
module unload gcc
module load gcc/7
module unload python
module load python/3.6.0
module unload cuda
module load cuda/102

for op in ${supportedOperations[@]}
do
  for dim in ${supportedDimensions[@]}
  do
    _op=($(echo $op | sed 'y/fdit/1234/'))
    _param_op=($(echo $_op | tr ' ' '\n'))
    param_op=($(echo $op | tr ' ' '\n'))
    param_dim=($(echo $dim | tr ' ' '\n'))

    if [ "$generateMatrices" = true ]; then
        fnameA="${param_dim[1]}_${param_dim[2]}_${param_op[1]}"
        fnameB="${param_dim[2]}_${param_dim[3]}_${param_op[2]}"

        if [[ $op == *"t"* ]]; then
          if (( $param_op[3] > 1 )); then
            fnameA="${fnameA}${param_op[3]}"
            fnameB="${fnameB}${param_op[3]}"
          fi
        fi

        fnameC="${fnameA}_${fnameB}"

        fnameA="${fnameA}.mlcuda"
        fnameB="${fnameB}.mlcuda"
        fnameC="${fnameC}.mlcuda"

        echo "[Batch Job]" generating ${fnameA} ...
        ../../bin/matgen ${param_dim[1]} ${param_dim[2]} ${param_op[1]} ${param_op[3]}
        echo "[Batch Job]" generating ${fnameB} ...
        ../../bin/matgen ${param_dim[2]} ${param_dim[3]} ${param_op[2]} ${param_op[3]}
        echo "[Batch Job]" generating ${fnameC} ...
        ../../bin/matgen ${fnameA} ${fnameB}

        cp -f $fnameA "../../data/matrices/${fnameA}"
        rm -f $fnameA
        if [ "$fnameA" != "$fnameB" ]; then
          cp -f $fnameB "../../data/matrices/${fnameB}"
          rm -f $fnameB
        fi
        cp -f $fnameC "../../data/matrices/${fnameC}"
        rm -f $fnameC
    fi

    echo "[Batch Job]" executing "python3 matmul.py ${dim} ${op} $indexDims" ...
    python3 matmul.py ${param_dim[1]} ${param_dim[2]} ${param_dim[3]}\
                      ${_param_op[1]} ${_param_op[2]} ${_param_op[3]}\
                      $indexDims

    if [ "$removeGeneratedMatricesAfterwards" = true ]; then
        rm -f "../../data/matrices/${fnameA}"
        rm -f "../../data/matrices/${fnameB}"
        rm -f "../../data/matrices/${fnameC}"
    fi
  done
done
