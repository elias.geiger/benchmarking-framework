#!/usr/bin/env python
import sys
import json
import numpy
import kernel_tuner
from collections import OrderedDict

def tune():

  #----------------------- configuration vars -----------------------#
  #-->  These get overriden if calling parameters are provided!   <--#

  size_m = 4
  size_k = 4
  size_n = 4

    # types: "f(loat)"=1, "d(ouble)"=2, "i(nterval)"=3, "t(angent)"=4
    # example: "itd"=342
  type_A = 1
  type_B = 31
  tangentVectorSize = 1

    # dimensions used by kernel: "x"=1, "x,y"=2, "x,y,z"=3
  dimsUsed = 2
  #------------------------------------------------------------------#

  if len(sys.argv) == 8:
    size_m = int(sys.argv[1])
    size_k = int(sys.argv[2])
    size_n = int(sys.argv[3])
    type_A = int(sys.argv[4])
    type_B = int(sys.argv[5])
    tangentVectorSize = int(sys.argv[6])
    dimsUsed = int(sys.argv[7])
  elif len(sys.argv) != 1:
    print('ERROR: wrong argument count provided: '+str(len(sys.argv)))
    print('Provided arguments are:')
    for i in range(len(sys.argv)):
      print(' '+sys.argv[i])
    print('--> use: matmul.py M K N typeA typeB tangentVectorSize dimsUsed')
    return

  if (dimsUsed == 1):
    problem_size = (size_m*size_n)
  elif (dimsUsed == 2):
    problem_size = (size_m, size_n)
  else:
    problem_size = (size_m, size_n)

  tune_params = OrderedDict()
  tune_params["block_size_x"] = [i+1 for i in range(64)]
  tune_params["block_size_y"] = [i+1 for i in range(64)]
  tune_params["active_dims"] = [dimsUsed]

  grid_div_x = ["block_size_x"]
  grid_div_y = ["block_size_y"]

  restrict = ["(block_size_x*block_size_y <= 1024) and ((block_size_x*block_size_y<=32) or ((block_size_x*block_size_y)%32==0))"]

  args = [numpy.int32(size_m), numpy.int32(size_k), numpy.int32(size_n),
    numpy.int32(type_A), numpy.int32(type_B), numpy.int32(tangentVectorSize)]

  result = kernel_tuner.tune_kernel("prepare_kernel", "matmul_wrapper.cu",
    problem_size, args, tune_params, grid_div_y=grid_div_y,
    grid_div_x=grid_div_x, restrictions=restrict,
    verbose=True, iterations=20, lang="C")

  fname = "../optimized/statistics_"
  fname += "m"+str(size_m)+"_"
  fname += "k"+str(size_k)+"_"
  fname += "n"+str(size_n)+"_"
  fname += "a"+str(type_A)+"_"
  fname += "b"+str(type_B)+"_"
  fname += "t"+str(tangentVectorSize)+"_"
  fname += "d"+str(dimsUsed)+".json"
  with open(fname, 'w') as file:
    json.dump(result, file)

if __name__ == "__main__":
  tune()
