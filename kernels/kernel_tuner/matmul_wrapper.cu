#include <sstream>
#include <limits>

#include "../../ml_cuda/ml_cuda.hpp"
#include "../tunable/matmul.cuh"


// ---------- select kernel and define maximal error delta ---------- //
#define kernel(NB,NT,A,B,C,b1,b2) kernels::matmul<<<NB,NT>>>(A,B,C,b1,b2)
#define DELTA double(pow(10, -5))
// ------------------------------------------------------------------ //


#ifndef grid_size_x
    #define grid_size_x 1
#endif
#ifndef grid_size_y
    #define grid_size_y 1
#endif
#ifndef block_size_x
    #define block_size_x 1
#endif
#ifndef block_size_y
    #define block_size_y 1
#endif

#if  block_size_x < block_size_y
  #define k_step block_size_x
#else
  #define k_step block_size_y
#endif

#define USED_SMEM_SIZE(a,b) block_size_x*block_size_y*(a+b)
#define SIZE_ERROR(a,b) (USED_SMEM_SIZE(a,b)>49152)

#define cfg_out "[Wrapper:gx="<<grid_size_x<<",bx="<<block_size_x<<",gy="<<grid_size_y<<",by="<<block_size_y<<"] "
static const float retInvalid = std::numeric_limits<float>::max();

template<typename TREAL_A, typename TREAL_B>
float _launch_kernel(const char* fname_A, const char* fname_B,
                     const char* fname_C, dim3 size) {

#ifdef active_dims
  #if active_dims == 1
    dim3 grid(grid_size_x);
    dim3 block(block_size_x);
  #elif active_dims == 2
    dim3 grid(grid_size_x, grid_size_y);
    dim3 block(block_size_x, block_size_y);
  #elif active_dims == 3
    dim3 grid(grid_size_x, grid_size_y, grid_size_z);
    dim3 block(block_size_x, block_size_y, block_size_z);
  #else
    std::cerr << cfg_out << "Wrong active_dims size: " << active_dims << "\n";
    return retInvalid;
  #endif
#else
  std::cerr << cfg_out << "ERROR: could not get active dimensions\n";
  dim3 grid(1);
  dim3 block(1);
  return retInvalid;
#endif

  ml_cuda::matrix_t<TREAL_A> A(fname_A, true);
  ml_cuda::matrix_t<TREAL_B> B(fname_B, true);
  ml_cuda::matrix_t<TREAL_B> C(size.x, size.z, true);
  ml_cuda::matrix_t<TREAL_B> C_ref(fname_C, false);

  index_t i=0;
  while (i<A.cols()) { i += k_step; }
  i -= k_step;
  index_t lastAccessAlignmentA = i;
  i=0;
  while (i<B.rows()) { i += k_step; }
  i -= k_step;
  index_t lastAccessAlignmentB = i;

  bool bounds_check = true;
  if (grid_size_x*block_size_x==C.rows() &&
      grid_size_y*block_size_y==C.cols() &&
      lastAccessAlignmentA+block_size_y==A.cols() &&
      lastAccessAlignmentB+block_size_x==B.rows()) {
    bounds_check = false;
  }
  if (!bounds_check) {
    std::cout << cfg_out << "Bounds check is omitted!\n";
  }

  cudaError_t err;
  cudaEvent_t start, stop;

  err = cudaEventCreate(&start);
  if (err != cudaSuccess) {
    std::cerr << cfg_out << "Cuda error in cudaEventCreate: "
              << cudaGetErrorString(err) << "\n";
  }
  err = cudaEventCreate(&stop);
  if (err != cudaSuccess) {
    std::cerr << cfg_out << "Cuda error in cudaEventCreate: "
              << cudaGetErrorString(err) << "\n";
  }

  cudaDeviceSetCacheConfig(cudaFuncCachePreferShared);

  cudaDeviceSynchronize();
  err = cudaGetLastError();
  if (err != cudaSuccess) {
    std::cerr << cfg_out << "Cuda error before kernel launch: "
              << cudaGetErrorString(err) << "\n";
  }

  cudaEventRecord(start);
  kernel(grid,block,A,B,C,false,bounds_check);
  cudaEventRecord(stop);
  cudaEventSynchronize(stop);

  float time = 0.0;
  err = cudaEventElapsedTime(&time, start, stop);
  if (err != cudaSuccess) {
    std::cerr << cfg_out << "Cuda error " << err << " after getting elapsed time: "
              << cudaGetErrorString(err) << "\n";
  }

  cudaDeviceSynchronize();
  err = cudaGetLastError();
  if (err != cudaSuccess) {
    const char *error_string = cudaGetErrorString(err);
    std::cout << cfg_out << "DEBUG: Cuda error " << err << " occured: " << error_string << "\n";
    if (strncmp("too many resources requested for launch", error_string, 10) == 0) {
      time = -1.0;
    } else {
      std::cerr << cfg_out << "Cuda error after kernel launch: "
                << cudaGetErrorString(err) << "\n";
      exit(1);
    }
  }

  double max_err = 0;
  for (unsigned int i = 0; i < size.x; ++i) {
    for (unsigned int j = 0; j < size.z; ++j) {
      double cur_err = ml_cuda::getError(C(i,j), C_ref(i,j));
      if (max_err < cur_err) max_err = cur_err;
    }
  }
  if (max_err > DELTA) {
    std::cerr << cfg_out << "Maximal relative error exceeds " << DELTA << ": " << max_err << "\n";
    time = retInvalid;
  }
  else if (max_err != 0){
    std::cout << cfg_out << "Maximal relative error is not zero: " << max_err << "\n";
  }

  return time;
}

void intToType(const unsigned int &inc, ml_cuda::type_t &out) {
  std::stringstream ss_type;
  int _inc = inc;
  while (_inc%10 != 0) {
    int cur;
    cur = _inc%10;
    switch (cur) {
    case 1: ss_type << 'f'; break;
    case 2: ss_type << 'd'; break;
    case 3: ss_type << 'i'; break;
    case 4: ss_type << 't'; break;
    default:
      std::cerr << cfg_out << "intToType could not parse: " << inc << "\n";
      break;
    }
    _inc /= 10;
  }
  std::string s_type(ss_type.str());
  std::string s_type_reversed(s_type.rbegin(), s_type.rend());
  ml_cuda::parseInputType(s_type_reversed.c_str(), out);
}

extern "C" float prepare_kernel(int size_m, int size_k, int size_n,
                    int type_A, int type_B, int tangentVectorSize) {
  float time;
  dim3 size(size_m, size_k, size_n);

  ml_cuda::type_t _type_A;
  intToType(type_A, _type_A);
  ml_cuda::type_t _type_B;
  intToType(type_B, _type_B);
  
  std::stringstream tvsExtension;
  if (tangentVectorSize > 1) tvsExtension << tangentVectorSize;

  std::stringstream ssFname_A;
  ssFname_A << "../../data/matrices/"
            << size.x << '_' << size.y << '_' << _type_A << tvsExtension.str() << ".mlcuda";
  std::stringstream ssFname_B;
  ssFname_B << "../../data/matrices/"
            << size.y << '_' << size.z << '_' << _type_B << tvsExtension.str() << ".mlcuda";
  std::stringstream ssFname_C;
  ssFname_C << "../../data/matrices/"
            << size.x << '_' << size.y << '_' << _type_A << tvsExtension.str() << '_'
            << size.y << '_' << size.z << '_' << _type_B << tvsExtension.str() << ".mlcuda";

  if (_type_A.primals[0] == ml_cuda::FLOAT &&
      _type_B.primals[0] == ml_cuda::FLOAT) {
    #if SIZE_ERROR(ML_CUDA_SIZE_FLOAT,ML_CUDA_SIZE_FLOAT)
    std::cerr << cfg_out << "Configuration needs to much shared memory: "
              << USED_SMEM_SIZE(ML_CUDA_SIZE_FLOAT,ML_CUDA_SIZE_FLOAT) << "/49152\n";
    return retInvalid;
    #else
    time = _launch_kernel<float, float>(
          ssFname_A.str().c_str(), ssFname_B.str().c_str(),
          ssFname_C.str().c_str(), size);
    #endif
  }
  else if (_type_A.primals[0] == ml_cuda::DOUBLE &&
           _type_B.primals[0] == ml_cuda::DOUBLE) {
    #if SIZE_ERROR(ML_CUDA_SIZE_DOUBLE,ML_CUDA_SIZE_DOUBLE)
    std::cerr << cfg_out << "Configuration needs to much shared memory: "
              << USED_SMEM_SIZE(ML_CUDA_SIZE_DOUBLE,ML_CUDA_SIZE_DOUBLE) << "/49152\n";
    return retInvalid;
    #else
    time = _launch_kernel<double, double>(
          ssFname_A.str().c_str(), ssFname_B.str().c_str(),
          ssFname_C.str().c_str(), size);
    #endif
  }
  else if (_type_A.primals[0] == ml_cuda::FLOAT &&
           _type_B.primals[0] == ml_cuda::INTERVAL &&
           _type_B.primals[1] == ml_cuda::FLOAT) {
    #if SIZE_ERROR(ML_CUDA_SIZE_FLOAT,ML_CUDA_SIZE_INTERVAL_FLOAT)
    std::cerr << cfg_out << "Configuration needs to much shared memory: "
              << USED_SMEM_SIZE(ML_CUDA_SIZE_FLOAT,ML_CUDA_SIZE_INTERVAL_FLOAT) << "/49152\n";
    return retInvalid;
    #else
    time = _launch_kernel<float, ml_cuda::interval_t<float>>(
          ssFname_A.str().c_str(), ssFname_B.str().c_str(),
          ssFname_C.str().c_str(), size);
    #endif
  }
  else if (_type_A.primals[0] == ml_cuda::DOUBLE &&
           _type_B.primals[0] == ml_cuda::INTERVAL &&
           _type_B.primals[1] == ml_cuda::DOUBLE) {
    #if SIZE_ERROR(ML_CUDA_SIZE_DOUBLE,ML_CUDA_SIZE_INTERVAL_DOUBLE)
    std::cerr << cfg_out << "Configuration needs to much shared memory: "
              << USED_SMEM_SIZE(ML_CUDA_SIZE_DOUBLE,ML_CUDA_SIZE_INTERVAL_DOUBLE) << "/49152\n";
    return retInvalid;
    #else
    time = _launch_kernel<double, ml_cuda::interval_t<double>>(
          ssFname_A.str().c_str(), ssFname_B.str().c_str(),
          ssFname_C.str().c_str(), size);
    #endif
  }
  else if (_type_A.primals[0] == ml_cuda::TANGENT &&
           _type_A.primals[1] == ml_cuda::FLOAT &&
           _type_B.primals[0] == ml_cuda::TANGENT &&
           _type_B.primals[1] == ml_cuda::FLOAT) {
    if (tangentVectorSize == 1) {
      #if SIZE_ERROR(ML_CUDA_SIZE_TANGENT_FLOAT,ML_CUDA_SIZE_TANGENT_FLOAT)
      std::cerr << cfg_out << "Configuration needs to much shared memory: "
                << USED_SMEM_SIZE(ML_CUDA_SIZE_TANGENT_FLOAT,ML_CUDA_SIZE_TANGENT_FLOAT) << "/49152\n";
      return retInvalid;
      #else
      time = _launch_kernel<ml_cuda::tangent_t<float>,
          ml_cuda::tangent_t<float>>(
            ssFname_A.str().c_str(), ssFname_B.str().c_str(),
            ssFname_C.str().c_str(), size);
      #endif
    }
    else if (tangentVectorSize == 4) {
      #if SIZE_ERROR(ML_CUDA_SIZE_TANGENT_FLOAT_4,ML_CUDA_SIZE_TANGENT_FLOAT_4)
      std::cerr << cfg_out << "Configuration needs to much shared memory: "
                << USED_SMEM_SIZE(ML_CUDA_SIZE_TANGENT_FLOAT_4,ML_CUDA_SIZE_TANGENT_FLOAT_4) << "/49152\n";
      return retInvalid;
      #else
      time = _launch_kernel<ml_cuda::tangent_t<float,4>,
          ml_cuda::tangent_t<float,4>>(
            ssFname_A.str().c_str(), ssFname_B.str().c_str(),
            ssFname_C.str().c_str(), size);
      #endif
    }
    else if (tangentVectorSize == 16) {
      #if SIZE_ERROR(ML_CUDA_SIZE_TANGENT_FLOAT_16,ML_CUDA_SIZE_TANGENT_FLOAT_16)
      std::cerr << cfg_out << "Configuration needs to much shared memory: "
                << USED_SMEM_SIZE(ML_CUDA_SIZE_TANGENT_FLOAT_16,ML_CUDA_SIZE_TANGENT_FLOAT_16) << "/49152\n";
      return retInvalid;
      #else
      time = _launch_kernel<ml_cuda::tangent_t<float,16>,
          ml_cuda::tangent_t<float,16>>(
            ssFname_A.str().c_str(), ssFname_B.str().c_str(),
            ssFname_C.str().c_str(), size);
      #endif
    }
    else if (tangentVectorSize == 64) {
      #if SIZE_ERROR(ML_CUDA_SIZE_TANGENT_FLOAT_64,ML_CUDA_SIZE_TANGENT_FLOAT_64)
      std::cerr << cfg_out << "Configuration needs to much shared memory: "
                << USED_SMEM_SIZE(ML_CUDA_SIZE_TANGENT_FLOAT_64,ML_CUDA_SIZE_TANGENT_FLOAT_64) << "/49152\n";
      return retInvalid;
      #else
      time = _launch_kernel<ml_cuda::tangent_t<float,64>,
          ml_cuda::tangent_t<float,64>>(
            ssFname_A.str().c_str(), ssFname_B.str().c_str(),
            ssFname_C.str().c_str(), size);
      #endif
    }
    else {
      std::cerr << cfg_out << "unsupported tangentVectorSize: "
                << tangentVectorSize << "\n";
      return retInvalid;
    }
  }
  else if (_type_A.primals[0] == ml_cuda::TANGENT &&
           _type_A.primals[1] == ml_cuda::DOUBLE &&
           _type_B.primals[0] == ml_cuda::TANGENT &&
           _type_B.primals[1] == ml_cuda::DOUBLE) {
    if (tangentVectorSize == 1) {
      #if SIZE_ERROR(ML_CUDA_SIZE_TANGENT_DOUBLE,ML_CUDA_SIZE_TANGENT_DOUBLE)
      std::cerr << cfg_out << "Configuration needs to much shared memory: "
                << USED_SMEM_SIZE(ML_CUDA_SIZE_TANGENT_DOUBLE,ML_CUDA_SIZE_TANGENT_DOUBLE) << "/49152\n";
      return retInvalid;
      #else
      time = _launch_kernel<ml_cuda::tangent_t<double>,
          ml_cuda::tangent_t<double>>(
            ssFname_A.str().c_str(), ssFname_B.str().c_str(),
            ssFname_C.str().c_str(), size);
      #endif
    }
    else if (tangentVectorSize == 4) {
      #if SIZE_ERROR(ML_CUDA_SIZE_TANGENT_DOUBLE_4,ML_CUDA_SIZE_TANGENT_DOUBLE_4)
      std::cerr << cfg_out << "Configuration needs to much shared memory: "
                << USED_SMEM_SIZE(ML_CUDA_SIZE_TANGENT_DOUBLE_4,ML_CUDA_SIZE_TANGENT_DOUBLE_4) << "/49152\n";
      return retInvalid;
      #else
      time = _launch_kernel<ml_cuda::tangent_t<double,4>,
          ml_cuda::tangent_t<double,4>>(
            ssFname_A.str().c_str(), ssFname_B.str().c_str(),
            ssFname_C.str().c_str(), size);
      #endif
    }
    else if (tangentVectorSize == 16) {
      #if SIZE_ERROR(ML_CUDA_SIZE_TANGENT_DOUBLE_16,ML_CUDA_SIZE_TANGENT_DOUBLE_16)
      std::cerr << cfg_out << "Configuration needs to much shared memory: "
                << USED_SMEM_SIZE(ML_CUDA_SIZE_TANGENT_DOUBLE_16,ML_CUDA_SIZE_TANGENT_DOUBLE_16) << "/49152\n";
      return retInvalid;
      #else
      time = _launch_kernel<ml_cuda::tangent_t<double,16>,
          ml_cuda::tangent_t<double,16>>(
            ssFname_A.str().c_str(), ssFname_B.str().c_str(),
            ssFname_C.str().c_str(), size);
      #endif
    }
    else if (tangentVectorSize == 64) {
      #if SIZE_ERROR(ML_CUDA_SIZE_TANGENT_DOUBLE_64,ML_CUDA_SIZE_TANGENT_DOUBLE_64)
      std::cerr << cfg_out << "Configuration needs to much shared memory: "
                << USED_SMEM_SIZE(ML_CUDA_SIZE_TANGENT_DOUBLE_64,ML_CUDA_SIZE_TANGENT_DOUBLE_64) << "/49152\n";
      return retInvalid;
      #else
      time = _launch_kernel<ml_cuda::tangent_t<double,64>,
          ml_cuda::tangent_t<double,64>>(
            ssFname_A.str().c_str(), ssFname_B.str().c_str(),
            ssFname_C.str().c_str(), size);
      #endif
    }
    else {
      std::cerr << cfg_out << "unsupported tangentVectorSize: "
                << tangentVectorSize << "\n";
      return retInvalid;
    }
  }
  else if (_type_A.primals[0] == ml_cuda::TANGENT &&
           _type_A.primals[1] == ml_cuda::FLOAT &&
           _type_B.primals[0] == ml_cuda::INTERVAL &&
           _type_B.primals[1] == ml_cuda::TANGENT &&
           _type_B.primals[2] == ml_cuda::FLOAT) {
    if (tangentVectorSize == 1) {
      #if SIZE_ERROR(ML_CUDA_SIZE_TANGENT_FLOAT,ML_CUDA_SIZE_INTERVAL_TANGENT_FLOAT)
      std::cerr << cfg_out << "Configuration needs to much shared memory: "
                << USED_SMEM_SIZE(ML_CUDA_SIZE_TANGENT_FLOAT,ML_CUDA_SIZE_INTERVAL_TANGENT_FLOAT) << "/49152\n";
      return retInvalid;
      #else
      time = _launch_kernel<ml_cuda::tangent_t<float>,
          ml_cuda::interval_t<ml_cuda::tangent_t<float>>>(
            ssFname_A.str().c_str(), ssFname_B.str().c_str(),
            ssFname_C.str().c_str(), size);
      #endif
    }
    else if (tangentVectorSize == 4) {
      #if SIZE_ERROR(ML_CUDA_SIZE_TANGENT_FLOAT_4,ML_CUDA_SIZE_INTERVAL_TANGENT_FLOAT_4)
      std::cerr << cfg_out << "Configuration needs to much shared memory: "
                << USED_SMEM_SIZE(ML_CUDA_SIZE_TANGENT_FLOAT_4,ML_CUDA_SIZE_INTERVAL_TANGENT_FLOAT_4) << "/49152\n";
      return retInvalid;
      #else
      time = _launch_kernel<ml_cuda::tangent_t<float,4>,
          ml_cuda::interval_t<ml_cuda::tangent_t<float,4>>>(
            ssFname_A.str().c_str(), ssFname_B.str().c_str(),
            ssFname_C.str().c_str(), size);
      #endif
    }
    else if (tangentVectorSize == 16) {
      #if SIZE_ERROR(ML_CUDA_SIZE_TANGENT_FLOAT_16,ML_CUDA_SIZE_INTERVAL_TANGENT_FLOAT_16)
      std::cerr << cfg_out << "Configuration needs to much shared memory: "
                << USED_SMEM_SIZE(ML_CUDA_SIZE_TANGENT_FLOAT_16,ML_CUDA_SIZE_INTERVAL_TANGENT_FLOAT_16) << "/49152\n";
      return retInvalid;
      #else
      time = _launch_kernel<ml_cuda::tangent_t<float,16>,
          ml_cuda::interval_t<ml_cuda::tangent_t<float,16>>>(
            ssFname_A.str().c_str(), ssFname_B.str().c_str(),
            ssFname_C.str().c_str(), size);
      #endif
    }
    else if (tangentVectorSize == 64) {
      #if SIZE_ERROR(ML_CUDA_SIZE_TANGENT_FLOAT_64,ML_CUDA_SIZE_INTERVAL_TANGENT_FLOAT_64)
      std::cerr << cfg_out << "Configuration needs to much shared memory: "
                << USED_SMEM_SIZE(ML_CUDA_SIZE_TANGENT_FLOAT_64,ML_CUDA_SIZE_INTERVAL_TANGENT_FLOAT_64) << "/49152\n";
      return retInvalid;
      #else
      time = _launch_kernel<ml_cuda::tangent_t<float,64>,
          ml_cuda::interval_t<ml_cuda::tangent_t<float,64>>>(
            ssFname_A.str().c_str(), ssFname_B.str().c_str(),
            ssFname_C.str().c_str(), size);
      #endif
    }
    else {
      std::cerr << cfg_out << "unsupported tangentVectorSize: "
                << tangentVectorSize << "\n";
      return retInvalid;
    }
  }
  else if (_type_A.primals[0] == ml_cuda::TANGENT &&
           _type_A.primals[1] == ml_cuda::DOUBLE &&
           _type_B.primals[0] == ml_cuda::INTERVAL &&
           _type_B.primals[1] == ml_cuda::TANGENT &&
           _type_B.primals[2] == ml_cuda::DOUBLE) {
    if (tangentVectorSize == 1) {
      #if SIZE_ERROR(ML_CUDA_SIZE_TANGENT_DOUBLE,ML_CUDA_SIZE_INTERVAL_TANGENT_DOUBLE)
      std::cerr << cfg_out << "Configuration needs to much shared memory: "
                << USED_SMEM_SIZE(ML_CUDA_SIZE_TANGENT_DOUBLE,ML_CUDA_SIZE_INTERVAL_TANGENT_DOUBLE) << "/49152\n";
      return retInvalid;
      #else
      time = _launch_kernel<ml_cuda::tangent_t<double>,
          ml_cuda::interval_t<ml_cuda::tangent_t<double>>>(
            ssFname_A.str().c_str(), ssFname_B.str().c_str(),
            ssFname_C.str().c_str(), size);
      #endif
    }
    else if (tangentVectorSize == 4) {
      #if SIZE_ERROR(ML_CUDA_SIZE_TANGENT_DOUBLE_4,ML_CUDA_SIZE_INTERVAL_TANGENT_DOUBLE_4)
      std::cerr << cfg_out << "Configuration needs to much shared memory: "
                << USED_SMEM_SIZE(ML_CUDA_SIZE_TANGENT_DOUBLE_4,ML_CUDA_SIZE_INTERVAL_TANGENT_DOUBLE_4) << "/49152\n";
      return retInvalid;
      #else
      time = _launch_kernel<ml_cuda::tangent_t<double,4>,
          ml_cuda::interval_t<ml_cuda::tangent_t<double,4>>>(
            ssFname_A.str().c_str(), ssFname_B.str().c_str(),
            ssFname_C.str().c_str(), size);
      #endif
    }
    else if (tangentVectorSize == 16) {
      #if SIZE_ERROR(ML_CUDA_SIZE_TANGENT_DOUBLE_16,ML_CUDA_SIZE_INTERVAL_TANGENT_DOUBLE_16)
      std::cerr << cfg_out << "Configuration needs to much shared memory: "
                << USED_SMEM_SIZE(ML_CUDA_SIZE_TANGENT_DOUBLE_16,ML_CUDA_SIZE_INTERVAL_TANGENT_DOUBLE_16) << "/49152\n";
      return retInvalid;
      #else
      time = _launch_kernel<ml_cuda::tangent_t<double,16>,
          ml_cuda::interval_t<ml_cuda::tangent_t<double,16>>>(
            ssFname_A.str().c_str(), ssFname_B.str().c_str(),
            ssFname_C.str().c_str(), size);
      #endif
    }
    else if (tangentVectorSize == 64) {
      #if SIZE_ERROR(ML_CUDA_SIZE_TANGENT_DOUBLE_64,ML_CUDA_SIZE_INTERVAL_TANGENT_DOUBLE_64)
      std::cerr << cfg_out << "Configuration needs to much shared memory: "
                << USED_SMEM_SIZE(ML_CUDA_SIZE_TANGENT_DOUBLE_64,ML_CUDA_SIZE_INTERVAL_TANGENT_DOUBLE_64) << "/49152\n";
      return retInvalid;
      #else
      time = _launch_kernel<ml_cuda::tangent_t<double,64>,
          ml_cuda::interval_t<ml_cuda::tangent_t<double,64>>>(
            ssFname_A.str().c_str(), ssFname_B.str().c_str(),
            ssFname_C.str().c_str(), size);
      #endif
    }
    else {
      std::cerr << cfg_out << "unsupported tangentVectorSize: "
                << tangentVectorSize << "\n";
      return retInvalid;
    }
  }
  else {
    std::cerr << cfg_out << "Type combination not supported\n";
    return retInvalid;
  }

  return time;
}
