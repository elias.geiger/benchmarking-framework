# Setup

If you are on a compute cluster supporting Environment Modules, load the correct modules:
```sh
$ module load DEVELOP
$ module unload intel
$ module unload gcc
$ module load gcc/7
$ module unload python
$ module load python/3.6.0
$ module unload cuda
$ module load cuda/102
```

To be able to compile the sources google benchmark is needed. Visit [Google Benchmark] and follow the installation instructions. Make sure to compile Google Benchmark with the same compiler that is used to compile these benchmark-framework sources (recommended: gcc 7).

Besides of google benchmark, you need to have the following installed for building the sources:
 * [CUDA toolkit] (recommended version: 10.2)
 * [cmake] (should already be present for building Google Benchmark)

In order to build the sources follow these steps:
```sh
$ git clone git@git.rwth-aachen.de:elias.geiger/benchmarking-framework.git
$ cd benchmarking-framework
```
Open CMakeLists.txt and point it to the google benchmark installation:
```cpp
#TODO point BENCHMARK to location of google benchmark
set(BENCHMARK "/home/elias/Programs/benchmark")
```
Build the project:
```sh
$ mkdir build && cd build
$ cmake ../
$ make
```

# Usage of the tuning capability

Make sure to install [kernel_tuner] first.
In order to start the tuning utility, follow these steps:

```sh
$ cd kernels/kernel_tuner
$ ./matmul_batch_local.sh
```

Vice versa on the cluster:
```sh
$ cd kernels/kernel_tuner
$ sbatch matmul_batch.sh
```
The resulting statistics are placed into /kernels/optimized


# Usage of the benchmarking capability

In order to start the benchmarking utility, follow these steps:
```sh
$ cd build
$ ./benchmark
```

Vice versa on the cluster:
```sh
$ cd benchmarks
$ sbatch gbench_batch.sh
```

The resulting statistics are placed into /benchmarks


[Google Benchmark]: https://github.com/google/benchmark
[kernel_tuner]: https://github.com/benvanwerkhoven/kernel_tuner
[CUDA toolkit]: https://developer.nvidia.com/cuda-downloads
[cmake]: https://cmake.org/
