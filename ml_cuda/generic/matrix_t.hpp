#ifndef GENERIC_MATRIX_HPP
#define GENERIC_MATRIX_HPP

#include <fstream>
#include <iostream>
#include <sstream>
#include <string.h>
#include "../traits/ml_cuda_traits.hpp"
#include "type_t.hpp"
#include <cuda.h>
#include <cuda_runtime_api.h>

namespace ml_cuda {

  enum transfer_modes { host_alloc_and_copy, unified_memory };

  template<typename TREAL> struct matrix_t {
    TREAL * __restrict _device_data = nullptr;
    TREAL * __restrict _host_data = nullptr;
    transfer_modes _transfer_mode;
    index_t _rows;
    index_t _cols;
    type_t _type;
    bool _is_device_data;
    bool _is_copy = false;

    matrix_t() {}

    matrix_t(const matrix_t &source) {
      _type = source._type;
      _host_data = source._host_data;
      _device_data = source._device_data;
      _rows = source._rows;
      _cols = source._cols;
      _is_device_data = source._is_device_data;
      _transfer_mode = source._transfer_mode;
      _is_copy = true;
    }

    // host alloc and init
    template <typename TLAMBDA>
    matrix_t(index_t rows, index_t cols, const TLAMBDA &init,
             bool paged_alloc = false) {
      _rows = rows;
      _cols = cols;
      _is_device_data = false;
      if (paged_alloc) {
        _transfer_mode = unified_memory;
        cudaError_t ret = cudaMallocManaged((void **)(&_device_data),
                                            sizeof(TREAL)*cols*rows);
        if (ret != 0) {
          std::cerr << "CUDA ERROR " << ret << " calling cudaMallocManaged\n";
        }
        _host_data = _device_data;
      } else {
        _transfer_mode = host_alloc_and_copy;
        _host_data = new TREAL[rows * cols];
      }
      for (index_t row = 0; row < _rows; ++row) {
        for (index_t col = 0; col < _cols; ++col) {
          _host_data[row * cols + col] = init(row, col);
        }
      }
    }

    // host alloc and dont init data
    matrix_t(index_t rows, index_t cols, bool paged_alloc = false) {
      _rows = rows;
      _cols = cols;
      _is_device_data = false;
      if (paged_alloc) {
        _transfer_mode = unified_memory;
        cudaError_t ret = cudaMallocManaged((void **)(&_device_data),
                                            sizeof(TREAL)*_cols*_rows);
        if (ret != 0) {
          std::cerr << "CUDA ERROR " << ret << " calling cudaMallocManaged\n";
        }
        _host_data = _device_data;
      } else {
        _transfer_mode = host_alloc_and_copy;
        _host_data = new TREAL[_rows * _cols];
      }
    }

    // read in from file
    matrix_t(const char* fname, bool paged_alloc = false) {
      std::ifstream f_in;
      unsigned int tangentVectorSize=1;
      type_t inc_type;
      parseHeader(fname, _rows, _cols, inc_type, tangentVectorSize);
      setType(inc_type);

      std::string dummy;
      std::fstream::pos_type pos;
      f_in.open(fname);
      std::getline(f_in, dummy, '\n');
      pos = dummy.length() + 1;
      f_in.close();

      _is_device_data = false;
      if (paged_alloc) {
        _transfer_mode = unified_memory;
        cudaError_t ret = cudaMallocManaged((void **)(&_device_data),
                                            sizeof(TREAL)*_cols*_rows);
        if (ret != 0) {
          std::cerr << "CUDA ERROR " << ret << " calling cudaMallocManaged\n";
        }
        _host_data = _device_data;
      } else {
        _transfer_mode = host_alloc_and_copy;
        _host_data = new TREAL[_rows * _cols];
      }

      f_in.open(fname, std::ifstream::binary);
      f_in.seekg(pos);
      f_in.read((char*)&_access(0), _rows*_cols*sizeof(TREAL));
      f_in.clear();
      f_in.close();
    }

    ~matrix_t() {
      if (_is_copy) return;

      if (_device_data != nullptr) {
        cudaError_t ret = cudaFree(_device_data);
        if (ret != 0) {
          std::cerr << "CUDA ERROR " << ret << " calling cudaFree on _device_data: "
                    << _device_data << " Reason: " << cudaGetErrorString(ret) << "\n";
        }
        _device_data = nullptr;
        if (_transfer_mode == unified_memory) _host_data = nullptr;
      }

      if (_host_data != nullptr) {
        delete[] _host_data;
        _host_data = nullptr;
      }
    }

    void setType(type_t inc_type) {
      _type = inc_type;
    }

    template<typename TLAMBDA>
    void iterateOrdered(const TLAMBDA &lambda) {
      for (index_t r=0; r<_rows; ++r) {
        for (index_t c=0; c<_cols; ++c) {
          TREAL& entry = _access(r,c);
          lambda(r,c,entry);
        }
      }
    }

    template<typename TLAMBDA>
    void iterateOrdered(const TLAMBDA &lambda) const {
      for (index_t r=0; r<_rows; ++r) {
        for (index_t c=0; c<_cols; ++c) {
          const TREAL& entry = operator()(r,c);
          lambda(r,c,entry);
        }
      }
    }

    template<typename TLAMBDA>
    void iterateUnordered(const TLAMBDA &lambda) {
      for (index_t i=0; i<_rows*_cols; ++i) {
        TREAL& entry = _access(i);
        lambda(entry);
      }
    }

    template<typename TLAMBDA>
    void iterateUnordered(const TLAMBDA &lambda) const {
      for (index_t i=0; i<_rows*_cols; ++i) {
        const TREAL& entry = operator()(i);
        lambda(entry);
      }
    }

    __host__ __device__ index_t rows() const { return _rows; }
    __host__ __device__ index_t cols() const { return _cols; }

    __host__ __device__ const TREAL& operator () (const index_t row,
                                                  const index_t col) const {
      #ifdef __CUDA_ARCH__
        return _device_data[row * _cols + col];
      #else
        return _host_data[row * _cols + col];
      #endif
    }

    __host__ __device__ const TREAL& operator () (const index_t pos) const {
      #ifdef __CUDA_ARCH__
        return _device_data[pos];
      #else
        return _host_data[pos];
      #endif
    }

    __host__ __device__ TREAL& _access(const index_t row, const index_t col) {
      #ifdef __CUDA_ARCH__
        return _device_data[row * _cols + col];
      #else
        return _host_data[row * _cols + col];
      #endif
    }

    __host__ __device__ TREAL& _access(const index_t pos) {
      #ifdef __CUDA_ARCH__
        return _device_data[pos];
      #else
        return _host_data[pos];
      #endif
    }

    // get safe returns copy of element, not a reference!
    __host__ __device__ const TREAL get_safe(const index_t row,
                                              const index_t col) const {
      if (_rows <= row || _cols <= col) {
        return TREAL(0);
      } else {
        return operator()(row, col);
      }
    }

    __host__ __device__ void store(const index_t row, const index_t col,
                                   const TREAL &val) {
      #ifdef __CUDA_ARCH__
        _device_data[row * _cols + col] = val;
      #else
        _host_data[row * _cols + col] = val;
      #endif
    }

    __host__ __device__ void store(const index_t pos, const TREAL &val) {
      #ifdef __CUDA_ARCH__
        _device_data[pos] = val;
      #else
        _host_data[pos] = val;
      #endif
    }

    __host__ __device__ void store_safe(const index_t row, const index_t col,
                                   const TREAL &val) {
      if (_rows <= row || _cols <= col) {
        return;
      } else {
        store(row, col, val);
      }
    }

    __host__ __device__ void transpose() {
      TREAL temp_TREAL;
      for (index_t r=0; r<_rows; r++) {
        for (index_t c=r+1; c<_cols; c++) {
          temp_TREAL = _access(r,c);
          _access(r,c) = _access(c,r);
          _access(c,r) = temp_TREAL;
        }
      }
      index_t temp_int = _rows;
      _rows = _cols;
      _cols = temp_int;
    }

    __host__ void copyDataToDevice() {
      if (_transfer_mode == unified_memory) return;
      cudaError_t ret;
      if (_device_data == nullptr) {
        ret = cudaMalloc((void **)(&_device_data), _rows*_cols*sizeof(TREAL));
        if (ret != 0) {
          std::cerr << "CUDA ERROR " << ret << " calling cudaMalloc\n";
        }
      }
      ret = cudaMemcpy(_device_data, _host_data, _rows*_cols*sizeof(TREAL),
                       cudaMemcpyHostToDevice);
      if (ret != 0) {
        std::cerr << "CUDA ERROR (copyDataToHost) " << ret << " calling cudaMemcpy\n";
      }
    }

    __host__ void copyDataToHost() {
      if (_transfer_mode == unified_memory) return;
      if (_device_data == nullptr) return;
      cudaError_t ret = cudaMemcpy(_host_data, _device_data, _rows*_cols*sizeof(TREAL),
                       cudaMemcpyDeviceToHost);
      if (ret != 0) {
        std::cerr << "CUDA ERROR (copyDataToHost) " << ret
                  << " calling cudaMemcpy on ptr _host_data: " << _host_data
                  << ", _device_data: " << _device_data
                  << ", reason: " << cudaGetErrorString(ret) << "\n";
      }
    }

    void writeToFile(const char* fname, unsigned int tangentVectorSize=1) {
      if (_is_device_data) return;
      std::ofstream f_out;
      f_out.open(fname, std::ofstream::trunc);
      f_out << "ML_CUDA " << _rows << ' ' << _cols << ' '
            << _type << ' ' << tangentVectorSize << "\n";
      f_out.close();
      f_out.open(fname, std::ofstream::app | std::ofstream::binary);
      f_out.write((char*)&_access(0), _rows*_cols*sizeof(TREAL));
    }
  };

  template<typename TREAL>
  std::ostream& operator << (std::ostream &os, const matrix_t<TREAL> &mat)
  {
    for (index_t row = 0; row < mat.rows(); ++row) {
      for (index_t col = 0; col < mat.cols(); ++col) {
        os << mat(row, col) << "\t";
      }
      os << "\n";
    }
    return os;
  }

  // traits
  namespace traits {

    template <typename TREAL>
    struct matrix_type_info {
      static const bool is_ml_cuda_matrix = true;
      typedef TREAL real_t;
    };

    template <typename TREAL>
    struct type_info<ml_cuda::matrix_t<TREAL>>
        : ml_cuda::traits::matrix_type_info<TREAL> {};

    template <typename TREAL>
    struct type_info<ml_cuda::matrix_t<TREAL> &>
        : ml_cuda::traits::matrix_type_info<TREAL> {};

    template <typename TREAL>
    struct type_info<const ml_cuda::matrix_t<TREAL>>
        : ml_cuda::traits::matrix_type_info<TREAL> {};

    template <typename TREAL>
    struct type_info<const ml_cuda::matrix_t<TREAL> &>
        : ml_cuda::traits::matrix_type_info<TREAL> {};

  } // namespace traits

} // namespace ml_cuda

#endif // GENERIC_MATRIX_HPP
