#ifndef TYPE_TYPE_HPP
#define TYPE_TYPE_HPP

#include <iostream>
#include <fstream>
#include <sstream>
#include <string.h>
#include <limits>
#include <cmath>

#include "../interval/interval_t.hpp"
#include "../tangent/tangent_t.hpp"

namespace ml_cuda {

enum primal_type_t {
  FLOAT = 'f',
  DOUBLE = 'd',
  INTERVAL = 'i',
  TANGENT = 't',
  UNSPECIFIED = 'n'
};

struct type_t {
  primal_type_t primals[4];

  type_t() {
    for (int i=0;i<4;++i) {
      primals[i] = UNSPECIFIED;
    }
  }
  type_t(type_t& src) {
    for (int i=0;i<4;++i) {
      primals[i] = src.primals[i];
    }
  }
  bool operator == (type_t &inc) const {
    for (int i=0;i<4;++i) {
      if (primals[i] != inc.primals[i]) return false;
    }
    return true;
  }
};

static inline std::ostream& operator << (std::ostream& os, const type_t& t)
{
  for (int i=0;i<4;++i) {
    if (t.primals[i]==UNSPECIFIED) return os;
    os << (char)t.primals[i];
  }
  return os;
}

template<typename T>
__host__ __device__ void print_t(const T) {
  printf("Error: Using non-specialized print_t...");
}

template<>
__host__ __device__ void print_t(const float t) {
  printf("%f", t);
}

template<>
__host__ __device__ void print_t(const double t) {
  printf("%f", t);
}

template<>
__host__ __device__ void print_t(const interval_t<float> t) {
  printf("[%f,%f]", t.getMin(), t.getMax());
}

template<>
__host__ __device__ void print_t(const interval_t<double> t) {
  printf("[%f,%f]", t.getMin(), t.getMax());
}

template<>
__host__ __device__ void print_t(const tangent_t<float> t) {
  printf("%f:<%f>", t.getValue(), t.getTangent());
}

template<>
__host__ __device__ void print_t(const tangent_t<float,4> t) {
  printf("%f:<%f,%f,%f,%f>", t.getValue(),
         t.getTangentAt(0),
         t.getTangentAt(1),
         t.getTangentAt(2),
         t.getTangentAt(3));
}

template<>
__host__ __device__ void print_t(const tangent_t<float,16> t) {
  printf("%f:<%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f>", t.getValue(),
         t.getTangentAt(0),
         t.getTangentAt(1),
         t.getTangentAt(2),
         t.getTangentAt(3),
         t.getTangentAt(4),
         t.getTangentAt(5),
         t.getTangentAt(6),
         t.getTangentAt(7),
         t.getTangentAt(8),
         t.getTangentAt(9),
         t.getTangentAt(10),
         t.getTangentAt(11),
         t.getTangentAt(12),
         t.getTangentAt(13),
         t.getTangentAt(14),
         t.getTangentAt(15));
}

template<>
__host__ __device__ void print_t(const tangent_t<double> t) {
  printf("%f:<%f>", t.getValue(), t.getTangent());
}

template<>
__host__ __device__ void print_t(const tangent_t<double,4> t) {
  printf("%f:<%f,%f,%f,%f>", t.getValue(),
         t.getTangentAt(0),
         t.getTangentAt(1),
         t.getTangentAt(2),
         t.getTangentAt(3));
}

template<>
__host__ __device__ void print_t(const tangent_t<double,16> t) {
  printf("%f:<%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f>", t.getValue(),
         t.getTangentAt(0),
         t.getTangentAt(1),
         t.getTangentAt(2),
         t.getTangentAt(3),
         t.getTangentAt(4),
         t.getTangentAt(5),
         t.getTangentAt(6),
         t.getTangentAt(7),
         t.getTangentAt(8),
         t.getTangentAt(9),
         t.getTangentAt(10),
         t.getTangentAt(11),
         t.getTangentAt(12),
         t.getTangentAt(13),
         t.getTangentAt(14),
         t.getTangentAt(15));
}

std::uniform_real_distribution<float> float_distribution(-pow(10,5), pow(10,5));
std::uniform_real_distribution<double> double_distribution(-pow(10,5), pow(10,5));
std::random_device rDevice;

template<typename T>
T init_random(index_t row, index_t col) {
  std::cerr << "ERROR: Using non-specialiced initialization\n";
  return row+col;
}

template<>
float init_random<float>(index_t row, index_t col) {
  return row+col+float_distribution(rDevice);
}

template<>
double init_random<double>(index_t row, index_t col) {
  return row+col+double_distribution(rDevice);
}

template<>
interval_t<float> init_random<interval_t<float>>(index_t row, index_t col) {
  return interval_t<float>(init_random<float>(row,col), init_random<float>(row,col));
}

template<>
interval_t<double> init_random<interval_t<double>>(index_t row, index_t col) {
  return interval_t<double>(init_random<double>(row, col), init_random<double>(row, col));
}

template<>
tangent_t<float> init_random<tangent_t<float>>(index_t row, index_t col) {
  return tangent_t<float>(init_random<float>(row,col), init_random<float>(row,col));
}

template<>
tangent_t<double> init_random<tangent_t<double>>(index_t row, index_t col) {
  return tangent_t<double>(init_random<double>(row, col), init_random<double>(row, col));
}

template<>
interval_t<tangent_t<float>> init_random<interval_t<tangent_t<float>>>(index_t row, index_t col) {
  return interval_t<tangent_t<float>>(init_random<tangent_t<float>>(row, col), init_random<tangent_t<float>>(row, col));
}

template<>
interval_t<tangent_t<double>> init_random<interval_t<tangent_t<double>>>(index_t row, index_t col) {
  return interval_t<tangent_t<double>>(init_random<tangent_t<double>>(row, col), init_random<tangent_t<double>>(row, col));
}

template<>
tangent_t<float,4> init_random<tangent_t<float,4>>(index_t row, index_t col) {
  float arr[4];
  for (int i=0;i<4;++i) {
    arr[i] = init_random<float>(row,col);
  }
  return tangent_t<float,4>(init_random<float>(row,col), arr);
}

template<>
tangent_t<float,16> init_random<tangent_t<float,16>>(index_t row, index_t col) {
  float arr[16];
  for (int i=0;i<16;++i) {
    arr[i] = init_random<float>(row,col);
  }
  return tangent_t<float,16>(init_random<float>(row,col), arr);
}

template<>
tangent_t<float,64> init_random<tangent_t<float,64>>(index_t row, index_t col) {
  float arr[64];
  for (int i=0;i<64;++i) {
    arr[i] = init_random<float>(row,col);
  }
  return tangent_t<float,64>(init_random<float>(row,col), arr);
}

template<>
tangent_t<double,4> init_random<tangent_t<double,4>>(index_t row, index_t col) {
  double arr[4];
  for (int i=0;i<4;++i) {
    arr[i] = init_random<double>(row,col);
  }
  return tangent_t<double,4>(init_random<double>(row,col), arr);
}

template<>
tangent_t<double,16> init_random<tangent_t<double,16>>(index_t row, index_t col) {
  double arr[16];
  for (int i=0;i<16;++i) {
    arr[i] = init_random<double>(row,col);
  }
  return tangent_t<double,16>(init_random<double>(row,col), arr);
}

template<>
tangent_t<double,64> init_random<tangent_t<double,64>>(index_t row, index_t col) {
  double arr[64];
  for (int i=0;i<64;++i) {
    arr[i] = init_random<double>(row,col);
  }
  return tangent_t<double,64>(init_random<double>(row,col), arr);
}

template<>
interval_t<tangent_t<float,4>> init_random<interval_t<tangent_t<float,4>>>(index_t row, index_t col) {
  return interval_t<tangent_t<float,4>>(init_random<tangent_t<float,4>>(row, col), init_random<tangent_t<float,4>>(row, col));
}

template<>
interval_t<tangent_t<float,16>> init_random<interval_t<tangent_t<float,16>>>(index_t row, index_t col) {
  return interval_t<tangent_t<float,16>>(init_random<tangent_t<float,16>>(row, col), init_random<tangent_t<float,16>>(row, col));
}

template<>
interval_t<tangent_t<float,64>> init_random<interval_t<tangent_t<float,64>>>(index_t row, index_t col) {
  return interval_t<tangent_t<float,64>>(init_random<tangent_t<float,64>>(row, col), init_random<tangent_t<float,64>>(row, col));
}

template<>
interval_t<tangent_t<double,4>> init_random<interval_t<tangent_t<double,4>>>(index_t row, index_t col) {
  return interval_t<tangent_t<double,4>>(init_random<tangent_t<double,4>>(row, col), init_random<tangent_t<double,4>>(row, col));
}

template<>
interval_t<tangent_t<double,16>> init_random<interval_t<tangent_t<double,16>>>(index_t row, index_t col) {
  return interval_t<tangent_t<double,16>>(init_random<tangent_t<double,16>>(row, col), init_random<tangent_t<double,16>>(row, col));
}

template<>
interval_t<tangent_t<double,64>> init_random<interval_t<tangent_t<double,64>>>(index_t row, index_t col) {
  return interval_t<tangent_t<double,64>>(init_random<tangent_t<double,64>>(row, col), init_random<tangent_t<double,64>>(row, col));
}

// calculate relative error. !Attention: left side (a) is expected reference value
template<typename T>
double getError(T a, T b) {
  std::cerr << "ERROR: Using non-specialized getError function\n";
  if ((T)a==(T)0.0) {
    return (double)std::fabs((T)(a-b));
  } else {
    return (double)std::fabs((T)(a-b))/(double)std::fabs((T)a);
  }
}

template<>
double getError<float>(float a, float b) {
  if ((float)a==(float)0.0) {
    return (double)std::fabs((float)(a-b));
  } else {
    return (double)std::fabs((float)(a-b))/(double)std::fabs((float)(a));
  }
}

template<>
double getError<double>(double a, double b) {
  if ((double)a==(double)0.0) {
    return (double)std::fabs((double)(a-b));
  } else {
    return (double)std::fabs((double)(a-b))/(double)std::fabs((double)(a));
  }
}

template<>
double getError<interval_t<float>>(interval_t<float> a, interval_t<float> b) {
  double err_min = getError(a.getMin(), b.getMin());
  double err_max = getError(a.getMax(), b.getMax());
  return (err_min < err_max) ? err_max : err_min;
}

template<>
double getError<interval_t<double>>(interval_t<double> a, interval_t<double> b) {
  double err_min = getError(a.getMin(), b.getMin());
  double err_max = getError(a.getMax(), b.getMax());
  return (err_min < err_max) ? err_max : err_min;
}

template<>
double getError<tangent_t<float>>(tangent_t<float> a, tangent_t<float> b) {
  double err_val = getError(a.getValue(), b.getValue());
  double err_tan = getError(a.getTangent(), b.getTangent());
  return (err_val < err_tan) ? err_tan : err_val;
}

template<>
double getError<tangent_t<double>>(tangent_t<double> a, tangent_t<double> b) {
  double err_val = getError(a.getValue(), b.getValue());
  double err_tan = getError(a.getTangent(), b.getTangent());
  return (err_val < err_tan) ? err_tan : err_val;
}

template<>
double getError<interval_t<tangent_t<float>>>(interval_t<tangent_t<float>> a, interval_t<tangent_t<float>> b) {
  double err_min = getError(a.getMin(), b.getMin());
  double err_max = getError(a.getMax(), b.getMax());
  return (err_min < err_max) ? err_max : err_min;
}

template<>
double getError<interval_t<tangent_t<double>>>(interval_t<tangent_t<double>> a, interval_t<tangent_t<double>> b) {
  double err_min = getError(a.getMin(), b.getMin());
  double err_max = getError(a.getMax(), b.getMax());
  return (err_min < err_max) ? err_max : err_min;
}

template<>
double getError<tangent_t<float,4>>(tangent_t<float,4> a, tangent_t<float,4> b) {
  double max_err = getError(a.getValue(), b.getValue());
  for (index_t i = 0; i < 4; ++i) {
    double cur_err = getError(a.getTangentAt(i), b.getTangentAt(i));
    if (max_err < cur_err) max_err = cur_err;
  }
  return max_err;
}

template<>
double getError<tangent_t<float,16>>(tangent_t<float,16> a, tangent_t<float,16> b) {
  double max_err = getError(a.getValue(), b.getValue());
  for (index_t i = 0; i < 16; ++i) {
    double cur_err = getError(a.getTangentAt(i), b.getTangentAt(i));
    if (max_err < cur_err) max_err = cur_err;
  }
  return max_err;
}

template<>
double getError<tangent_t<float,64>>(tangent_t<float,64> a, tangent_t<float,64> b) {
  double max_err = getError(a.getValue(), b.getValue());
  for (index_t i = 0; i < 64; ++i) {
    double cur_err = getError(a.getTangentAt(i), b.getTangentAt(i));
    if (max_err < cur_err) max_err = cur_err;
  }
  return max_err;
}

template<>
double getError<tangent_t<double,4>>(tangent_t<double,4> a, tangent_t<double,4> b) {
  double max_err = getError(a.getValue(), b.getValue());
  for (index_t i = 0; i < 4; ++i) {
    double cur_err = getError(a.getTangentAt(i), b.getTangentAt(i));
    if (max_err < cur_err) max_err = cur_err;
  }
  return max_err;
}

template<>
double getError<tangent_t<double,16>>(tangent_t<double,16> a, tangent_t<double,16> b) {
  double max_err = getError(a.getValue(), b.getValue());
  for (index_t i = 0; i < 16; ++i) {
    double cur_err = getError(a.getTangentAt(i), b.getTangentAt(i));
    if (max_err < cur_err) max_err = cur_err;
  }
  return max_err;
}

template<>
double getError<tangent_t<double,64>>(tangent_t<double,64> a, tangent_t<double,64> b) {
  double max_err = getError(a.getValue(), b.getValue());
  for (index_t i = 0; i < 64; ++i) {
    double cur_err = getError(a.getTangentAt(i), b.getTangentAt(i));
    if (max_err < cur_err) max_err = cur_err;
  }
  return max_err;
}

template<>
double getError<interval_t<tangent_t<float,4>>>(interval_t<tangent_t<float,4>> a, interval_t<tangent_t<float,4>> b) {
  double err_min = getError(a.getMin(), b.getMin());
  double err_max = getError(a.getMax(), b.getMax());
  return (err_min < err_max) ? err_max : err_min;
}

template<>
double getError<interval_t<tangent_t<float,16>>>(interval_t<tangent_t<float,16>> a, interval_t<tangent_t<float,16>> b) {
  double err_min = getError(a.getMin(), b.getMin());
  double err_max = getError(a.getMax(), b.getMax());
  return (err_min < err_max) ? err_max : err_min;
}

template<>
double getError<interval_t<tangent_t<float,64>>>(interval_t<tangent_t<float,64>> a, interval_t<tangent_t<float,64>> b) {
  double err_min = getError(a.getMin(), b.getMin());
  double err_max = getError(a.getMax(), b.getMax());
  return (err_min < err_max) ? err_max : err_min;
}

template<>
double getError<interval_t<tangent_t<double,4>>>(interval_t<tangent_t<double,4>> a, interval_t<tangent_t<double,4>> b) {
  double err_min = getError(a.getMin(), b.getMin());
  double err_max = getError(a.getMax(), b.getMax());
  return (err_min < err_max) ? err_max : err_min;
}

template<>
double getError<interval_t<tangent_t<double,16>>>(interval_t<tangent_t<double,16>> a, interval_t<tangent_t<double,16>> b) {
  double err_min = getError(a.getMin(), b.getMin());
  double err_max = getError(a.getMax(), b.getMax());
  return (err_min < err_max) ? err_max : err_min;
}

template<>
double getError<interval_t<tangent_t<double,64>>>(interval_t<tangent_t<double,64>> a, interval_t<tangent_t<double,64>> b) {
  double err_min = getError(a.getMin(), b.getMin());
  double err_max = getError(a.getMax(), b.getMax());
  return (err_min < err_max) ? err_max : err_min;
}

static inline void parseInputType(const char* in, type_t& out) {
  if (strcmp(in, "f") == 0) {
    out.primals[0] = ml_cuda::FLOAT;
  }
  else if (strcmp(in, "d") == 0) {
    out.primals[0] = ml_cuda::DOUBLE;
  }
  else if (strcmp(in, "if") == 0) {
    out.primals[0] = ml_cuda::INTERVAL;
    out.primals[1] = ml_cuda::FLOAT;
  }
  else if (strcmp(in, "id") == 0) {
    out.primals[0] = ml_cuda::INTERVAL;
    out.primals[1] = ml_cuda::DOUBLE;
  }
  // first order types
  else if (strcmp(in, "tf") == 0) {
    out.primals[0] = ml_cuda::TANGENT;
    out.primals[1] = ml_cuda::FLOAT;
  }
  else if (strcmp(in, "td") == 0) {
    out.primals[0] = ml_cuda::TANGENT;
    out.primals[1] = ml_cuda::DOUBLE;
  }
  else if (strcmp(in, "itf") == 0) {
    out.primals[0] = ml_cuda::INTERVAL;
    out.primals[1] = ml_cuda::TANGENT;
    out.primals[2] = ml_cuda::FLOAT;
  }
  else if (strcmp(in, "itd") == 0) {
    out.primals[0] = ml_cuda::INTERVAL;
    out.primals[1] = ml_cuda::TANGENT;
    out.primals[2] = ml_cuda::DOUBLE;
  }
  else {
    std::cerr << "ERROR: Type not supported or bad format\n";
    exit(1);
  }
}

bool parseHeader(const char *fname, index_t &rows, index_t &cols, type_t &type, unsigned int &tangentVectorSize) {
  std::ifstream f_in;
  std::string meta_string;
  std::stringstream meta_stream;
  std::string meta_data[5];

  f_in.open(fname);
  std::getline(f_in, meta_string, '\n');
  meta_stream << meta_string;
  for (int i = 0; i < 5; ++i) {
    std::getline(meta_stream, meta_data[i], ' ');
  }
  f_in.close();
  if (strcmp(meta_data[0].c_str(), "ML_CUDA") != 0) {
    std::cerr << "Error: Data is no ML_CUDA matrix, type=\'"
              << meta_data[0].c_str() << "\'\n";
    return false;
  }

  rows = std::atoi(meta_data[1].c_str());
  cols = std::atoi(meta_data[2].c_str());
  if (rows < 1 || cols < 1) {
    std::cerr << "Error: Meta-data provides empty matrix\n";
    return false;
  }
  parseInputType(meta_data[3].c_str(), type);
  tangentVectorSize = std::atoi(meta_data[4].c_str());
  return true;
}

} // namespace ml_cuda

#endif // TYPE_TYPE_HPP
