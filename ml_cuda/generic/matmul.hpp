#ifndef GENERIC_MATMUL_HPP
#define GENERIC_MATMUL_HPP

#include <type_traits>
#include <device_launch_parameters.h>

#include "../traits/ml_cuda_traits.hpp"

namespace ml_cuda {

  template<typename MATRIX_A, typename MATRIX_B, typename MATRIX_C,
           typename TI_A = ml_cuda::traits::type_info<MATRIX_A>,
           typename TI_B = ml_cuda::traits::type_info<MATRIX_B>,
           typename TI_C = ml_cuda::traits::type_info<MATRIX_C>,
           typename check_A = typename std::enable_if<TI_A::is_ml_cuda_matrix>::type,
           typename check_B = typename std::enable_if<TI_B::is_ml_cuda_matrix>::type,
           typename check_C = typename std::enable_if<TI_C::is_ml_cuda_matrix>::type>
  __global__ void matmul_reference(const MATRIX_A A, const MATRIX_B B, MATRIX_C C, bool transposeA=false) {

    typedef typename TI_C::real_t REAL_T;
    index_t row, col;
    REAL_T lsum;

    // grid stride loop
    for (index_t i=blockIdx.x*blockDim.x+threadIdx.x;
         i<(transposeA ? A.cols():A.rows())*B.cols();
         i+=blockDim.x*gridDim.x) {
      row = i / B.cols();
      col = i % B.cols();
      lsum = REAL_T(0);

      for (index_t j=0; j<B.rows(); j++) {
        lsum += (transposeA ? A(j,row):A(row,j))*B(j,col);
      }
      C.store(row, col, lsum);
    }
  }
}

#endif // GENERIC_MATMUL_HPP
