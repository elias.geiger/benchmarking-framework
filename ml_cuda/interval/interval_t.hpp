#ifndef INTERVAL_TYPE_HPP
#define INTERVAL_TYPE_HPP

#include "../traits/ml_cuda_traits.hpp"

#include <stdlib.h>
#include <cuda_runtime.h>
#include <iostream>
#include <random>
#include <limits>

namespace ml_cuda {

  template <typename TREAL>
  struct interval_t {
    TREAL _min;
    TREAL _max;

    __host__ __device__ interval_t(const TREAL &min, const TREAL &max) {
      if (max<min) {
        _min = max;
        _max = min;
      }
      else {        
        _min = min;
        _max = max;
      }
    }

    __host__ __device__ interval_t() {}

    __host__ __device__ interval_t(unsigned int init) :
      _min(TREAL(init)), _max(TREAL(init)) {}

    __host__ __device__ const TREAL& getMin() const {return _min;}
    __host__ __device__ const TREAL& getMax() const {return _max;}

    __host__ __device__ interval_t& operator += (const interval_t &iv) {
      this->_min += iv._min;
      this->_max += iv._max;
      return *this;
    }

    __host__ __device__ void inline print_t() const {
      printf("[");
      print(_min);
      printf(",");
      print(_max);
      printf("]");
    }
  };

  template <typename TREAL>
  __host__ std::ostream& operator << (std::ostream& os, const interval_t<TREAL>& iv)
  {
    os << '[' << iv._min << ',' << iv._max << ']';
    return os;
  }

  template <typename TREAL>
  __host__ __device__ interval_t<TREAL> operator * (const TREAL &s, const interval_t<TREAL> &iv) {
    return (s < 0) ? interval_t<TREAL>(s*iv._max,s*iv._min) : interval_t<TREAL>(s*iv._min,s*iv._max);
  }

  // traits
  namespace traits {

    template <typename TREAL>
    struct interval_type_info {
      static const bool is_ml_cuda_type = true;
      typedef TREAL real_t;
    };

    template <typename TREAL>
    struct type_info<ml_cuda::interval_t<TREAL>>
        : ml_cuda::traits::interval_type_info<TREAL> {};

    template <typename TREAL>
    struct type_info<ml_cuda::interval_t<TREAL> &>
        : ml_cuda::traits::interval_type_info<TREAL> {};

    template <typename TREAL>
    struct type_info<const ml_cuda::interval_t<TREAL>>
        : ml_cuda::traits::interval_type_info<TREAL> {};

    template <typename TREAL>
    struct type_info<const ml_cuda::interval_t<TREAL> &>
        : ml_cuda::traits::interval_type_info<TREAL> {};

  } // namespace traits

} // namespace ml_cuda

#endif // INTERVAL_TYPE_HPP
