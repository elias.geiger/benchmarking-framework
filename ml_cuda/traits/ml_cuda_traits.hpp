#ifndef ML_CUDA_TRAITS_HPP
#define ML_CUDA_TRAITS_HPP

#include <cuda_runtime.h>
#include <cstdio>

namespace ml_cuda {

  template <typename T>
  __host__ __device__ inline void print(const T &t) {
    t.print_t();
  }

  template <>
  __host__ __device__ inline void print(const float &t) {
    printf("%f", t);
  }

  template <>
  __host__ __device__ inline void print(const double &t) {
    printf("%f", t);
  }

  template <typename T>
  __host__ __device__ inline void println(const T &t) {
    print(t);
    printf("\n");
  }

  typedef unsigned int index_t;

  namespace traits {

    template<typename T>
    struct type_info {
      static const bool is_ml_cuda_matrix = false;
      static const bool is_ml_cuda_type = false;
    };

  }

}

#endif // ML_CUDA_TRAITS_HPP
