﻿#ifndef SEPARATE_TANGENT_MATRIX_HPP
#define SEPARATE_TANGENT_MATRIX_HPP

#include "../generic/matrix_t.hpp"
#include "../traits/ml_cuda_traits.hpp"

#include "tangent_t.hpp"

namespace ml_cuda {

template<typename T, unsigned TANGENT_VECTOR_SIZE=1>
struct separate_tangent_matrix_t {

  matrix_t<T> _valuesMatrix;
  matrix_t<T> _tangentsMatrices[TANGENT_VECTOR_SIZE];
  separate_tangent_matrix_t<T, TANGENT_VECTOR_SIZE>* _dev_ptr = nullptr;

  separate_tangent_matrix_t(matrix_t<T> &valuesMatrix, matrix_t<T> *tangentsMatrices)
    : _valuesMatrix(valuesMatrix) {
    for (unsigned int i=0; i<TANGENT_VECTOR_SIZE; ++i) {
      if (valuesMatrix.rows() != tangentsMatrices[i].rows() ||
          valuesMatrix.cols() != tangentsMatrices[i].cols()) {
        std::cerr << "ERROR: Value and Tangent Matrix differ in dimensions "
                  << "in separate tangent matrix ctor\n";
        exit(1);
      }
      _tangentsMatrices[i] = tangentsMatrices[i]; 
      _tangentsMatrices[i]._is_copy = true;
    }
    _valuesMatrix._is_copy = true;
  }

  __host__ __device__ index_t rows() const { return _valuesMatrix.rows(); }
  __host__ __device__ index_t cols() const { return _valuesMatrix.cols(); }

  __host__ void copyDataToDevice() {
    _valuesMatrix.copyDataToDevice();
    for (unsigned int i=0; i<TANGENT_VECTOR_SIZE; ++i) {
      _tangentsMatrices[i].copyDataToDevice();
    }

    if (_dev_ptr != nullptr) {
      cudaFree(_dev_ptr);
      _dev_ptr = nullptr;
    }
  }

  __host__ void copyInstanceToDevice() {
    cudaError_t err;
    err = cudaMallocManaged(&_dev_ptr, sizeof(*this));
    if(err!=0) std::cerr << "Cuda error " << err
                         << " on copyInstanceToDevice (malloc): "
                         << cudaGetErrorString(err) << "\n";
    err = cudaMemcpy(_dev_ptr, this, sizeof(*this), cudaMemcpyHostToDevice);
    if(err!=0) std::cerr << "Cuda error " << err
                         << " on copyInstanceToDevice: "
                         << cudaGetErrorString(err) << "\n";
  }

  __host__ separate_tangent_matrix_t<T, TANGENT_VECTOR_SIZE>*
  getDeviceInstance() {
    return _dev_ptr;
  }

  __host__ void copyDataToHost() {
    _valuesMatrix.copyDataToHost();
    for (unsigned int i=0; i<TANGENT_VECTOR_SIZE; ++i) {
      _tangentsMatrices[i].copyDataToHost();
    }
  }

  __host__ void deleteInstanceFromDevice() {
    if (_dev_ptr == nullptr) return;
    cudaError_t err;
    err = cudaFree(_dev_ptr);
    if(err!=0) std::cerr << "Cuda error before copyDataToHost cudaFree: " << err
                         << ": " << cudaGetErrorString(err) << "\n";
    _dev_ptr = nullptr;
  }

  __host__ __device__ void store(const index_t row, const index_t col,
                                 const tangent_t<T, TANGENT_VECTOR_SIZE> &val) {
    _valuesMatrix.store(row, col, val.getValue());
    for (unsigned int i=0; i<TANGENT_VECTOR_SIZE; ++i) {
      _tangentsMatrices[i].store(row, col, val.getTangentAt(i));
    }
  }

  __host__ __device__ void store_safe(const index_t row, const index_t col,
                                      const tangent_t<T, TANGENT_VECTOR_SIZE> &val) {
    _valuesMatrix.store_safe(row, col, val.getValue());
    for (unsigned int i=0; i<TANGENT_VECTOR_SIZE; ++i) {
      _tangentsMatrices[i].store_safe(row, col, val.getTangentAt(i));
    }
  }

  tangent_t<T, TANGENT_VECTOR_SIZE>
  __host__ __device__ operator () (index_t row, index_t col) const {
    T arr[TANGENT_VECTOR_SIZE];
    for (unsigned int i=0; i<TANGENT_VECTOR_SIZE; ++i) {
      arr[i] = _tangentsMatrices[i](row, col);
    }
    return tangent_t<T,TANGENT_VECTOR_SIZE>(_valuesMatrix(row, col), arr);
  }

  __host__ __device__ tangent_t<T,TANGENT_VECTOR_SIZE>
  get_safe(const index_t row, const index_t col) const {
    T arr[TANGENT_VECTOR_SIZE];
    for (unsigned int i=0; i<TANGENT_VECTOR_SIZE; ++i) {
      arr[i] = _tangentsMatrices[i].get_safe(row, col);
    }
    return tangent_t<T,TANGENT_VECTOR_SIZE>(_valuesMatrix.get_safe(row, col), arr);
  }
};

template<typename T, unsigned int TANGENT_VECTOR_SIZE=1>
__host__ std::ostream& operator << (std::ostream &os, const separate_tangent_matrix_t<T,TANGENT_VECTOR_SIZE> &mat) {
  for (index_t row = 0; row < mat._valuesMatrix.rows(); ++row) {
    for (index_t col = 0; col < mat._valuesMatrix.cols(); ++col) {
      T arr[TANGENT_VECTOR_SIZE];
      for (unsigned int i=0; i<TANGENT_VECTOR_SIZE; ++i) {
        arr[i] = mat._tangentsMatrices[i](row, col);
      }
      os << tangent_t<T,TANGENT_VECTOR_SIZE>(mat._valuesMatrix(row, col), arr);
      os << "\t";
    }
    os << "\n";
  }
  return os;
}

namespace traits
{

template<typename T, unsigned TANGENT_VECTOR_SIZE=1>
struct e_separate_tangent_matrix_type_info  {
  static const bool is_ml_cuda_matrix = true;
  typedef tangent_t<T,TANGENT_VECTOR_SIZE> real_t;
};

template<typename T, unsigned TANGENT_VECTOR_SIZE>
struct type_info<ml_cuda::separate_tangent_matrix_t<T,TANGENT_VECTOR_SIZE>> :
    ml_cuda::traits::e_separate_tangent_matrix_type_info<T,TANGENT_VECTOR_SIZE> {};

template<typename T, unsigned TANGENT_VECTOR_SIZE>
struct type_info<const ml_cuda::separate_tangent_matrix_t<T,TANGENT_VECTOR_SIZE>> :
    ml_cuda::traits::e_separate_tangent_matrix_type_info<T,TANGENT_VECTOR_SIZE> {};

template<typename T, unsigned TANGENT_VECTOR_SIZE>
struct type_info<ml_cuda::separate_tangent_matrix_t<T,TANGENT_VECTOR_SIZE>&> :
    ml_cuda::traits::e_separate_tangent_matrix_type_info<T,TANGENT_VECTOR_SIZE> {};

template<typename T, unsigned TANGENT_VECTOR_SIZE>
struct type_info<const ml_cuda::separate_tangent_matrix_t<T,TANGENT_VECTOR_SIZE>&> :
    ml_cuda::traits::e_separate_tangent_matrix_type_info<T,TANGENT_VECTOR_SIZE> {};
} // namespace traits

} // namespace ml_cuda

#endif // SEPARATE_TANGENT_MATRIX_HPP
