#ifndef TANGENT_TYPE_HPP
#define TANGENT_TYPE_HPP

#include <cuda_runtime.h>
#include <iostream>

#include "../traits/ml_cuda_traits.hpp"

using ml_cuda::index_t;

namespace ml_cuda {

template<typename T_VALUE, typename T_TANGENT, unsigned int TANGENT_VECTOR_SIZE>
struct baseTangent_t {
  T_VALUE _value;
  T_TANGENT _tangents[TANGENT_VECTOR_SIZE];

  __host__ __device__ baseTangent_t() {}

  __host__ __device__ baseTangent_t(unsigned int init) : _value(T_VALUE(init)) {
    for (unsigned int i=0; i<TANGENT_VECTOR_SIZE; ++i) {
      _tangents[i] = T_TANGENT(init);
    }
  }

  __host__ __device__ baseTangent_t(const T_VALUE& value,
                                    const T_TANGENT* tangents) : _value(value) {
    for (unsigned int i=0; i<TANGENT_VECTOR_SIZE; ++i) {
      _tangents[i] = tangents[i];
    }
  }

  __host__ __device__ const T_VALUE& getValue() const {return _value;}
  __host__ __device__ const T_TANGENT& getTangent() const {return _tangents[0];}
  __host__ __device__ const T_TANGENT* getTangents() const {return _tangents;}
  __host__ __device__ const T_TANGENT& getTangentAt(index_t index) const {
    return _tangents[index];
  }

  __host__ __device__ baseTangent_t<T_VALUE, T_TANGENT, TANGENT_VECTOR_SIZE>&
  operator += (const baseTangent_t<T_VALUE, T_TANGENT, TANGENT_VECTOR_SIZE>&
               inc) {
    this->_value += inc._value;
    for (unsigned int i=0; i<TANGENT_VECTOR_SIZE; ++i) {
      this->_tangents[i] += inc._tangents[i];
    }
    return *this;
  }

  __host__ __device__ bool operator <
  (const baseTangent_t<T_VALUE, T_TANGENT, TANGENT_VECTOR_SIZE>& inc) const {
    return _value < inc._value;
  }

  __host__ __device__ bool operator <
  (const T_VALUE& inc) const {
    return _value < inc;
  }

  __host__ __device__ void inline print_t() const {
    print(_value);
    printf(":<");
    print(_tangents[0]);
    for (index_t i=1; i<TANGENT_VECTOR_SIZE; ++i) {
      printf(",");
      print(_tangents[i]);
    }
    printf(">");
  }
};

// tangent vector mode
template<typename T, unsigned int TANGENT_VECTOR_SIZE=1>
struct tangent_t : baseTangent_t<T,T,TANGENT_VECTOR_SIZE> {
  __host__ __device__ tangent_t() : baseTangent_t<T,T,TANGENT_VECTOR_SIZE>() {}
  __host__ __device__ tangent_t(unsigned int init) : baseTangent_t<T,T,TANGENT_VECTOR_SIZE>(init) {}
  __host__ __device__ tangent_t(const T& value, const T* tangents) :
    baseTangent_t<T,T,TANGENT_VECTOR_SIZE>(value, tangents) {}
};

// simple tangent (template specialization of tangent vector mode)
template<typename T>
struct tangent_t<T,1> : baseTangent_t<T,T,1> {
  __host__ __device__ tangent_t() : baseTangent_t<T,T,1>() {}
  __host__ __device__ tangent_t(unsigned int init) : baseTangent_t<T,T,1>(init) {}
  __host__ __device__ tangent_t(const T& value, const T& tangent) :
    baseTangent_t<T,T,1>(value, &tangent) {}
  __host__ __device__ tangent_t(const T& value, const T* tangent) :
    baseTangent_t<T,T,1>(value, tangent) {}
};

template<typename T_VALUE, typename T_TANGENT,
         unsigned int TANGENT_VECTOR_SIZE=1>
__host__ std::ostream&
operator << (std::ostream &os,
             const baseTangent_t<T_VALUE,T_TANGENT,TANGENT_VECTOR_SIZE> &tn) {
  os << tn._value << ":<" << tn._tangents[0];
  for (unsigned int i=1; i<TANGENT_VECTOR_SIZE; ++i) {
    os << "," << tn._tangents[i];
  }
  os << '>';
  return os;
}

template<typename T_VALUE, typename T_TANGENT,
         unsigned int TANGENT_VECTOR_SIZE=1>
inline __host__ __device__ baseTangent_t<T_VALUE,T_TANGENT,TANGENT_VECTOR_SIZE>
operator * (const T_VALUE &a,
            const baseTangent_t<T_VALUE,T_TANGENT,TANGENT_VECTOR_SIZE> &b) {
  T_TANGENT arr[TANGENT_VECTOR_SIZE];
  for(unsigned int i=0; i<TANGENT_VECTOR_SIZE; ++i) {
    arr[i] = a*b._tangents[i];
  }
  return baseTangent_t<T_VALUE,T_TANGENT,TANGENT_VECTOR_SIZE>(a*b._value, arr);
}

template<typename T, unsigned int TANGENT_VECTOR_SIZE=1>
inline __host__ __device__ tangent_t<T,TANGENT_VECTOR_SIZE>
operator * (const T &a,
            const baseTangent_t<T,T,TANGENT_VECTOR_SIZE> &b) {
  T arr[TANGENT_VECTOR_SIZE];
  for(unsigned int i=0; i<TANGENT_VECTOR_SIZE; ++i) {
    arr[i] = a*b._tangents[i];
  }
  return tangent_t<T,TANGENT_VECTOR_SIZE>(a*b._value, arr);
}

template<typename T>
inline __host__ __device__ tangent_t<T>
operator * (const T &a,
            const baseTangent_t<T,T,1> &b) {
  return tangent_t<T>(a*b._value, a*b.getTangent());
}

template<typename T_VALUE, typename T_TANGENT,
         unsigned int TANGENT_VECTOR_SIZE=1>
inline __host__ __device__ baseTangent_t<T_VALUE,T_TANGENT,TANGENT_VECTOR_SIZE>
operator * (const baseTangent_t<T_VALUE,T_TANGENT,TANGENT_VECTOR_SIZE> &a,
            const baseTangent_t<T_VALUE,T_TANGENT,TANGENT_VECTOR_SIZE> &b) {
  T_VALUE value = a._value*b._value;
  T_TANGENT tangents[TANGENT_VECTOR_SIZE];
  for(unsigned int i=0; i<TANGENT_VECTOR_SIZE; ++i) {
    tangents[i] = a._value*b._tangents[i] + a._tangents[i]*b._value;
  }
  return baseTangent_t<T_VALUE,T_TANGENT,TANGENT_VECTOR_SIZE>(value, tangents);
}

template<typename T, unsigned int TANGENT_VECTOR_SIZE=1>
inline __host__ __device__ tangent_t<T,TANGENT_VECTOR_SIZE>
operator * (const baseTangent_t<T,T,TANGENT_VECTOR_SIZE> &a,
            const baseTangent_t<T,T,TANGENT_VECTOR_SIZE> &b) {
  T value = a._value*b._value;
  T tangents[TANGENT_VECTOR_SIZE];
  for(unsigned int i=0; i<TANGENT_VECTOR_SIZE; ++i) {
    tangents[i] = a._value*b._tangents[i] + a._tangents[i]*b._value;
  }
  return tangent_t<T,TANGENT_VECTOR_SIZE>(value, tangents);
}

template<typename T>
inline __host__ __device__ tangent_t<T>
operator * (const baseTangent_t<T,T,1> &a,
            const baseTangent_t<T,T,1> &b) {
  T value = a._value*b._value;
  T tangent = a._value*b.getTangent() + a.getTangent()*b._value;
  return tangent_t<T>(value, tangent);
}

template<typename T_VALUE, typename T_TANGENT,
         unsigned int TANGENT_VECTOR_SIZE=1,
         typename check_size =
         typename std::enable_if< (TANGENT_VECTOR_SIZE>1) >::type>
inline __host__ __device__ baseTangent_t<T_VALUE,T_TANGENT,TANGENT_VECTOR_SIZE>
operator * (const baseTangent_t<T_VALUE,T_TANGENT,1> &a,
            const baseTangent_t<T_VALUE,T_TANGENT,TANGENT_VECTOR_SIZE> &b) {
  T_VALUE value = a._value*b._value;
  T_TANGENT tangents[TANGENT_VECTOR_SIZE];
  for(unsigned int i=0; i<TANGENT_VECTOR_SIZE; ++i) {
    tangents[i] = a._value*b._tangents[i] + a._tangents[0]*b._value;
  }
  return baseTangent_t<T_VALUE,T_TANGENT,TANGENT_VECTOR_SIZE>(value, tangents);
}

template<typename T, unsigned int TANGENT_VECTOR_SIZE=1,
         typename check_size =
         typename std::enable_if< (TANGENT_VECTOR_SIZE>1) >::type>
inline __host__ __device__ tangent_t<T,TANGENT_VECTOR_SIZE>
operator * (const baseTangent_t<T,T,1> &a,
            const baseTangent_t<T,T,TANGENT_VECTOR_SIZE> &b) {
  T value = a._value*b._value;
  T tangents[TANGENT_VECTOR_SIZE];
  for(unsigned int i=0; i<TANGENT_VECTOR_SIZE; ++i) {
    tangents[i] = a._value*b._tangents[i] + a._tangents[0]*b._value;
  }
  return tangent_t<T,TANGENT_VECTOR_SIZE>(value, tangents);
}

namespace traits
{

template<typename TPRIMAL, typename TTANGENT, unsigned TANGENT_VECTOR_SIZE=1>
struct tangent_type_info  {
  static const bool is_ml_cuda_type = true;
  typedef TPRIMAL real_t_primal;
  typedef TTANGENT real_t_tangent;
};

template<typename TPRIMAL, typename TTANGENT, unsigned TANGENT_VECTOR_SIZE>
struct type_info<ml_cuda::baseTangent_t<TPRIMAL, TTANGENT, TANGENT_VECTOR_SIZE>> :
    ml_cuda::traits::tangent_type_info<TPRIMAL, TTANGENT, TANGENT_VECTOR_SIZE> {};

template<typename TPRIMAL, typename TTANGENT, unsigned TANGENT_VECTOR_SIZE>
struct type_info<const ml_cuda::baseTangent_t<TPRIMAL, TTANGENT, TANGENT_VECTOR_SIZE>> :
    ml_cuda::traits::tangent_type_info<TPRIMAL, TTANGENT, TANGENT_VECTOR_SIZE> {};

template<typename TPRIMAL, typename TTANGENT, unsigned TANGENT_VECTOR_SIZE>
struct type_info<ml_cuda::baseTangent_t<TPRIMAL, TTANGENT, TANGENT_VECTOR_SIZE>&> :
    ml_cuda::traits::tangent_type_info<TPRIMAL, TTANGENT, TANGENT_VECTOR_SIZE> {};

template<typename TPRIMAL, typename TTANGENT, unsigned TANGENT_VECTOR_SIZE>
struct type_info<const ml_cuda::baseTangent_t<TPRIMAL, TTANGENT, TANGENT_VECTOR_SIZE>&> :
    ml_cuda::traits::tangent_type_info<TPRIMAL, TTANGENT, TANGENT_VECTOR_SIZE> {};


template<typename TREAL, unsigned TANGENT_VECTOR_SIZE=1>
struct e_tangent_type_info  {
  static const bool is_ml_cuda_type = true;
  typedef TREAL real_t;
};

template<typename TREAL, unsigned TANGENT_VECTOR_SIZE>
struct type_info<ml_cuda::tangent_t<TREAL,TANGENT_VECTOR_SIZE>> :
    ml_cuda::traits::e_tangent_type_info<TREAL,TANGENT_VECTOR_SIZE> {};

template<typename TREAL, unsigned TANGENT_VECTOR_SIZE>
struct type_info<const ml_cuda::tangent_t<TREAL,TANGENT_VECTOR_SIZE>> :
    ml_cuda::traits::e_tangent_type_info<TREAL,TANGENT_VECTOR_SIZE> {};

template<typename TREAL, unsigned TANGENT_VECTOR_SIZE>
struct type_info<ml_cuda::tangent_t<TREAL,TANGENT_VECTOR_SIZE>&> :
    ml_cuda::traits::e_tangent_type_info<TREAL,TANGENT_VECTOR_SIZE> {};

template<typename T, unsigned TANGENT_VECTOR_SIZE>
struct type_info<const ml_cuda::tangent_t<T,TANGENT_VECTOR_SIZE>&> :
    ml_cuda::traits::e_tangent_type_info<T,TANGENT_VECTOR_SIZE> {};
} // namespace traits

} // namespace ml_cuda

#endif // TANGENT_TYPE_HPP
